FROM amazoncorretto:17.0.6

COPY target/arrosage-0.0.1-SNAPSHOT.jar arrosage_app.jar

ENTRYPOINT ["java", "-jar", "arrosage_app.jar"]