DROP TABLE IF EXISTS gardiennage;
DROP TABLE IF EXISTS conseil;
DROP TABLE IF EXISTS commentaire;
DROP TABLE IF EXISTS photo;
DROP TABLE IF EXISTS plante;
DROP TABLE IF EXISTS botaniste;
DROP TABLE IF EXISTS utilisateur;
DROP TABLE IF EXISTS adresse;


CREATE TABLE utilisateur
(
    id                   int AUTO_INCREMENT NOT NULL,
    utilisateur_mail     VARCHAR(255),
    utilisateur_login    VARCHAR(255),
    utilisateur_password VARCHAR(255),
    utilisateur_role     VARCHAR(50),
    PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE plante
(
    plante_id        int AUTO_INCREMENT NOT NULL,
    plante_nom       VARCHAR(255),
    plante_categorie VARCHAR(255),
    adresse_id       INT                NOT NULL,
    PRIMARY KEY (plante_id)
) ENGINE = InnoDB;


CREATE TABLE photo
(
    photo_id         int AUTO_INCREMENT NOT NULL,
    photo_descriptif VARCHAR(255),
    photo_date       DATE,
    photo_image      LONGBLOB,
    plante_id        INT                NOT NULL,
    utilisateur_id   INT                NOT NULL,
    PRIMARY KEY (photo_id)
) ENGINE = InnoDB;


CREATE TABLE botaniste
(
    id                   int AUTO_INCREMENT NOT NULL,
    botaniste_specialite VARCHAR(100),
    PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE commentaire
(
    commentaire_id      int AUTO_INCREMENT NOT NULL,
    commentaire_message TEXT(1000),
    photo_id            INT                NOT NULL,
    PRIMARY KEY (commentaire_id)
) ENGINE = InnoDB;


CREATE TABLE adresse
(
    adresse_id      int AUTO_INCREMENT NOT NULL,
    adresse_point_x DOUBLE,
    adresse_point_y DOUBLE,
    PRIMARY KEY (adresse_id)
) ENGINE = InnoDB;


CREATE TABLE gardiennage
(
    gardiennage_id         INT AUTO_INCREMENT NOT NULL,
    plante_id              int                NOT NULL,
    gardiennage_date_debut DATE,
    gardiennage_date_fin   DATE,
    gardiennage_proprio    INT                NOT NULL,
    gardiennage_gardien    INT,
    PRIMARY KEY (gardiennage_id)
) ENGINE = InnoDB;


CREATE TABLE conseil
(
    conseil_id   INT AUTO_INCREMENT NOT NULL,
    botaniste_id INT                NOT NULL,
    photo_id     int                NOT NULL,
    conseil_avis VARCHAR(250),
    PRIMARY KEY (conseil_id)
) ENGINE = InnoDB;

ALTER TABLE plante
    ADD CONSTRAINT FK_Plante_adresse_id FOREIGN KEY (adresse_id) REFERENCES adresse (adresse_id);
ALTER TABLE photo
    ADD CONSTRAINT FK_Photo_plante_id FOREIGN KEY (plante_id) REFERENCES plante (plante_id);
ALTER TABLE photo
    ADD CONSTRAINT FK_Photo_utilisateur_id FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id);
ALTER TABLE botaniste
    ADD CONSTRAINT FK_Botaniste_utilisateur_id FOREIGN KEY (id) REFERENCES utilisateur (id);
ALTER TABLE commentaire
    ADD CONSTRAINT FK_Commentaire_photo_id FOREIGN KEY (photo_id) REFERENCES photo (photo_id);
ALTER TABLE gardiennage
    ADD CONSTRAINT FK_Gardiennage_plante_id FOREIGN KEY (plante_id) REFERENCES plante (plante_id);
ALTER TABLE gardiennage
    ADD CONSTRAINT FK_Gardiennage_gardiennage_proprio FOREIGN KEY (gardiennage_proprio) REFERENCES utilisateur (id);
ALTER TABLE gardiennage
    ADD CONSTRAINT FK_Gardiennage_gardiennage_gardien FOREIGN KEY (gardiennage_gardien) REFERENCES utilisateur (id);
ALTER TABLE conseil
    ADD CONSTRAINT FK_Conseil_photo_id FOREIGN KEY (photo_id) REFERENCES photo (photo_id);
ALTER TABLE conseil
    ADD CONSTRAINT FK_Conseil_botaniste_id FOREIGN KEY (botaniste_id) REFERENCES botaniste (id);
