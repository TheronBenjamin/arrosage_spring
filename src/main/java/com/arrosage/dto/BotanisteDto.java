package com.arrosage.dto;

import com.arrosage.model.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BotanisteDto {
    private Integer id;
    private String specialite;
    private String mail;
    private String login;
    private String password;
    private Role role;
    private List<Integer> gardiennageProprietaireIds;
    private List<Integer> gardiennageGardienIds;
    private List<Integer> photosIds;
    private List<ConseilDto> conseils;
}
