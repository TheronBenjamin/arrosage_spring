package com.arrosage.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {

    private Integer id;
    private Date dateEnvoi;
    private String body;
    private int emeteur;
    private int recepteur;

}
