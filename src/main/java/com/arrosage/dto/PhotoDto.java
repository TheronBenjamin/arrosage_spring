package com.arrosage.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PhotoDto {
    private Integer id;
    private String descriptif;
    private Date datePrisePhoto;
    private byte[] image;
    private Integer planteId;
    private String planteNom;
    private String photographeMail;
    private Integer photographeId;
    private List<String> conseilsAvis;
    private List<Integer> conseilsIds ;
}
