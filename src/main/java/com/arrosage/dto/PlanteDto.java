package com.arrosage.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlanteDto {
    private Integer id;
    private String denomination;
    private String categorie;
    private AdresseDto adresse;
    private List<Integer> gardiennageIds;
    private List<Integer> photosIds ;
}
