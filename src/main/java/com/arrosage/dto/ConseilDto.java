package com.arrosage.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConseilDto {
    private Integer id;
    private Integer botanisteId;
    private Integer photoId;
    private String avis;
}
