package com.arrosage.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GardiennageDto {
    private Integer id;
    private Date dateDebut;
    private Date dateFin;
    private Integer proprietaireId;
    private String proprietaireMail;
    private Integer gardienId;
    private String  gardienMail;
    private PlanteDto plante;
}
