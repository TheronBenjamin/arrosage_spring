package com.arrosage.exceptions.gardiennage.exception;

public class GardiennageDateException extends Exception {
    public GardiennageDateException() {
        super("La date n'est pas renseignée");
    }

    public GardiennageDateException(String message) {
        super(message);
    }
}
