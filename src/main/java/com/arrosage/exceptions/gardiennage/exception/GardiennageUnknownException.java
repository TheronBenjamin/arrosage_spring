package com.arrosage.exceptions.gardiennage.exception;

public class GardiennageUnknownException extends Exception {
    public GardiennageUnknownException() {
        super("Cette mission de gardiennage n'est pas connue");
    }

    public GardiennageUnknownException(String message) {
        super(message);
    }
}
