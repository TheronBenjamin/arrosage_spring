package com.arrosage.exceptions.gardiennage.exception;

public class GardiennageProprioException extends Exception {
    public GardiennageProprioException() {
        super("le propriétaire de la plante n'est pas renseignée");
    }

    public GardiennageProprioException(String message) {
        super(message);
    }
}
