package com.arrosage.exceptions;

public class BotanisteUnknownException extends Exception{

    public BotanisteUnknownException(String msg) {
        super(msg);
    }

    @Override
    public String getMessage() {
        return "Botaniste not found!";
    }
}
