package com.arrosage.exceptions;

public class MesageNotFoundException extends RuntimeException{
    public MesageNotFoundException(String msg) {
        super(msg);
    }

    public MesageNotFoundException() {super("Ce message n'existe pas");}

}
