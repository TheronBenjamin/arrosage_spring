package com.arrosage.exceptions;

public class UtilisateurException extends RuntimeException{

    public UtilisateurException() {
        super("User not found");
    }

    public UtilisateurException(String message) {
        super(message);
    }
}
