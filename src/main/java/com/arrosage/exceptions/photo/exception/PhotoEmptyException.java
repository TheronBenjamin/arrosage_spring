package com.arrosage.exceptions.photo.exception;

public class PhotoEmptyException extends Exception {

    public PhotoEmptyException() {
        super("Aucune photo n'a été trouvé");
    }

    public PhotoEmptyException(String msg) {
        super(msg);
    }

    
}
