package com.arrosage.exceptions.photo.exception;

public class PhotoDateException extends Exception {
    public PhotoDateException() {
        super("La date n'est pas correcte");
    }

    public PhotoDateException(String msg) {
        super(msg);
    }
}
