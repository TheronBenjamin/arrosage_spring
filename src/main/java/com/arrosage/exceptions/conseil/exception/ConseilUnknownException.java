package com.arrosage.exceptions.conseil.exception;

public class ConseilUnknownException extends Exception {
    public ConseilUnknownException() {
        super("ce conseil est inconnue");
    }

    public ConseilUnknownException(String message) {
        super(message);
    }
}
