package com.arrosage.exceptions.conseil.exception;

public class ConseilAvisException extends Exception {
    public ConseilAvisException() {
        super("Ce conseil n'a pas de message");
    }

    public ConseilAvisException(String message) {
        super(message);
    }
}
