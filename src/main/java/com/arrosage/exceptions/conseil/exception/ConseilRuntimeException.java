package com.arrosage.exceptions.conseil.exception;

public class ConseilRuntimeException extends RuntimeException {
    public ConseilRuntimeException() {
        super();
    }

    public ConseilRuntimeException(String message) {
        super(message);
    }
}
