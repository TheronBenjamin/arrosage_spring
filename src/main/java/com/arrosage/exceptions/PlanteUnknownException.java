package com.arrosage.exceptions;

public class PlanteUnknownException extends Exception{

    public PlanteUnknownException(String msg) {
        super(msg);
    }

   
}
