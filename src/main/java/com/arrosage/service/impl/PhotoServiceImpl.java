package com.arrosage.service.impl;

import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.UtilisateurException;
import com.arrosage.exceptions.photo.exception.PhotoDateException;
import com.arrosage.model.Conseil;
import com.arrosage.model.Photo;
import com.arrosage.model.Plante;
import com.arrosage.model.Utilisateur;
import com.arrosage.repository.PhotoRepository;
import com.arrosage.repository.PlanteRepository;
import com.arrosage.repository.UtilisateurRepository;
import com.arrosage.service.ConseilService;
import com.arrosage.service.PhotoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoRepository photoRepository;

    private final PlanteRepository planteRepository;
    private final UtilisateurRepository utilisateurRepository;
    private final ConseilService conseilService;
    Logger log = LoggerFactory.getLogger(PhotoServiceImpl.class);

    public PhotoServiceImpl(PhotoRepository photoRepository, ConseilServiceImpl conseilService, PlanteRepository planteRepository, UtilisateurRepository utilisateurRepository) {
        this.photoRepository = photoRepository;
        this.conseilService = conseilService;
        this.planteRepository = planteRepository;
        this.utilisateurRepository = utilisateurRepository;
    }

    @Override
    public List<Photo> getAll() {
        return this.photoRepository.findAll();
    }

    @Override
    public Photo getById(Integer id) {
        return this.photoRepository.getReferenceById(id);
    }

    /**
     * Permet de d'ajouter une photo, nécessite une date, une plante et un utilisateur
     *
     * @param photo
     * @return
     * @throws PhotoDateException
     * @throws PlanteUnknownException
     * @throws UtilisateurException
     */
    @Override
    public Photo createPhoto(Photo photo) throws PhotoDateException, PlanteUnknownException, UtilisateurException {
        Photo photoToAdd = new Photo();
        log.info("Attempting to create new photo");
        if (photo.getDescriptif() != null) {
            photoToAdd.setDescriptif(photo.getDescriptif());
        }

        photoToAdd.setPlante(this.getPlante(photo));

        if (photo.getDatePrisePhoto() != null && !photo.getDatePrisePhoto().after(Date.from(Instant.now()))) {
            photoToAdd.setDatePrisePhoto(photo.getDatePrisePhoto());
        } else {
            log.error("pas de date pour cette photo");
            throw new PhotoDateException();
        }

        photoToAdd.setPhotographe(this.getPhotographe(photo));

        photoToAdd.setConseils(null);

        //TODO:Gestion de l'image 
        photoToAdd.setImage(photo.getImage());
        log.info("Attempting to save new photo");
        return this.photoRepository.save(photoToAdd);
    }

    /**
     * Permet de modifier le descriptif de la photo
     *
     * @param photo
     * @return
     * @throws PhotoDateException
     * @throws PlanteUnknownException
     * @throws UtilisateurException
     */
    @Override
    public Photo updatePhoto(Photo photo) throws PhotoDateException, PlanteUnknownException, UtilisateurException {
        Photo photoToUpdate = new Photo();
        if (photo.getDescriptif() != null) {
            photoToUpdate.setDescriptif(photo.getDescriptif());
        }

        photoToUpdate.setPlante(this.getPlante(photo));


        if (photo.getDatePrisePhoto() != null && !photo.getDatePrisePhoto().after(Date.from(Instant.now()))) {
            photoToUpdate.setDatePrisePhoto(photo.getDatePrisePhoto());
        } else {
            throw new PhotoDateException();
        }

        photoToUpdate.setPhotographe(this.getPhotographe(photo));

        if (photo.getConseils() != null && !photo.getConseils().isEmpty()) {
            List<Conseil> conseils = this.conseilService.getConseilsByPhoto(photo);
            photoToUpdate.setConseils(conseils);
        } else {
            photoToUpdate.setConseils(null);
        }

        //TODO:Gestion de l'image méthode private
        photoToUpdate.setImage(photo.getImage());

        return this.photoRepository.save(photoToUpdate);
    }

    public List<Photo> getPhotosByUtilisateur(Utilisateur utilisateur) {
        return this.photoRepository.findPhotosByPhotographeId(utilisateur.getId());
    }

    public List<Photo> getPhotosByPlante(Plante plante) {
        return this.photoRepository.findPhotosByPlanteId(plante.getId());
    }

    private Plante getPlante(Photo photo) throws PlanteUnknownException {
        Plante plante = null;
        if (photo.getPlante() != null) {
            try {
                plante = this.planteRepository.getReferenceById(photo.getPlante().getId());
            } catch (EntityNotFoundException e) {
                throw new PlanteUnknownException("cette plante est inconnue");
            }
        } else {
            throw new PlanteUnknownException("La photo n'est pas associé à une plante");
        }
        return plante;
    }

    private Utilisateur getPhotographe(Photo photo) throws UtilisateurException {
        Utilisateur utilisateur;
        if (photo.getPhotographe() != null) {
            try {
                utilisateur = this.utilisateurRepository.getReferenceById(photo.getPhotographe().getId());
            } catch (EntityNotFoundException e) {
                throw new UtilisateurException("cet utilisateur est inconnu");
            }
        } else {
            throw new UtilisateurException("Cette photo n'a pas de propriétaire");
        }
        return utilisateur;
    }
}
