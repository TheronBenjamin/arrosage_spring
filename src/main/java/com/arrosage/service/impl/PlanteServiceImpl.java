package com.arrosage.service.impl;

import com.arrosage.exceptions.AdresseException;
import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.photo.exception.PhotoEmptyException;
import com.arrosage.model.Adresse;
import com.arrosage.model.Gardiennage;
import com.arrosage.model.Photo;
import com.arrosage.model.Plante;
import com.arrosage.repository.PlanteRepository;
import com.arrosage.service.AdresseService;
import com.arrosage.service.GardiennageService;
import com.arrosage.service.PhotoService;
import com.arrosage.service.PlanteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanteServiceImpl implements PlanteService {


    private final PlanteRepository planteRepository;


    //Be careful to not import PlanteService in AdresseService (Circular dependency risk)
    private final AdresseService adresseService;

    private final PhotoService photoService;

    private final GardiennageService gardiennageService;
    Logger log = LoggerFactory.getLogger(PlanteServiceImpl.class);

    public PlanteServiceImpl(PlanteRepository planteRepository, AdresseService adresseService, PhotoServiceImpl photoService, GardiennageServiceImpl gardiennageService) {
        this.planteRepository = planteRepository;
        this.adresseService = adresseService;
        this.photoService = photoService;
        this.gardiennageService = gardiennageService;

    }

    @Override
    public List<Plante> getAll() {
        return this.planteRepository.findAll();
    }

    @Override
    public Plante getById(Integer id) {
        return this.planteRepository.getReferenceById(id);
    }


    /**
     * Permet de créer une plante, nécessite d'ajouter l'adresse en même temps.
     *
     * @param plante
     * @return Plante
     * @throws AdresseException
     */
    @Override
    public Plante createPlante(Plante plante) throws AdresseException {
        Plante planteToCreate = new Plante();
        if (plante.getCategorie() != null) {
            planteToCreate.setCategorie(plante.getCategorie());
        }

        if (plante.getDenomination() != null) {
            planteToCreate.setDenomination(plante.getDenomination());
        }

        log.info("Attempting to create a plante");
        if (plante.getAdresse() != null) {
            Integer adresseId = this.createAdresseAccordingToNewPlante(plante.getAdresse()).getId();
            plante.getAdresse().setId(adresseId);
            planteToCreate.setAdresse(plante.getAdresse());
        } else {
            throw new AdresseException("l'adresse n'est pas renseignée");
        }


        planteToCreate.setGardes(null);

        if (this.isOneOrMorePhotos(plante)) {
            //TODO: Il est nécessaire de créer la plante avant d'y ajouter des photos. A voir comment cela s'organise côté IHM
            log.info("a la création de la plante des photos ont été ajouté");
            List<Photo> photos = this.photoService.getPhotosByPlante(plante);
            planteToCreate.setPhotos(photos);
        } else {
            planteToCreate.setPhotos(null);
        }

        log.info("Attempting to save the new plante");
        return this.planteRepository.save(planteToCreate);
    }


    /**
     * Permet de modifier la plante, le comportement diffère si la plante a déjà été gardé ou non.
     *
     * @param plante
     * @return Plante
     * @throws PhotoEmptyException
     * @throws AdresseException
     * @throws PlanteUnknownException
     */
    @Override
    public Plante updatePlante(Plante plante) throws PhotoEmptyException, AdresseException, PlanteUnknownException {

        if (plante.getCategorie() != null && plante.getDenomination() != null) {
            plante.setCategorie(plante.getCategorie());
            plante.setDenomination(plante.getDenomination());
        } else {
            throw new PlanteUnknownException("cette plante n'a pas de dénomination");
        }

        if (plante.getAdresse() != null) {
            plante.setAdresse(plante.getAdresse());
        } else {
            throw new AdresseException("L'adresse est mal renseignée");
        }

        if (this.aDejaEteGarde(plante)) {
            log.info("la plante a déjà été gardé");
            List<Gardiennage> gardiennages = this.gardiennageService.getGardiennagesByPlante(plante);
            plante.setGardes(gardiennages);
        } else {
            plante.setGardes(null);
        }

        //Si la plante a déjà été gardé elle a au moins une photo donc si pas de photo -> Exception

        if (this.isOneOrMorePhotos(plante)) {
            log.info("recupération de la liste de photos");
            List<Photo> photos = this.photoService.getPhotosByPlante(plante);
            plante.setPhotos(photos);
        } else {
            if (!this.aDejaEteGarde(plante)) {
                log.info("pas de photo mais la plante n'a pas encore été gardé");
                plante.setPhotos(null);
            } else {
                log.error("la plante a déjà été gardé, il doit y avoir au moins une photo");
                throw new PhotoEmptyException();
            }
        }

        return this.planteRepository.save(plante);
    }

    private Adresse createAdresseAccordingToNewPlante(Adresse adresse) throws AdresseException {
        log.info("Création de l'adresse lors d'un ajout de plante");
        try {
            return this.adresseService.createAdresse(adresse);
        } catch (Exception e) {
            throw new AdresseException("L'adreses est mal renseignée");
        }
    }

    private boolean aDejaEteGarde(Plante plante) {
        log.info("Vérification si la plante a déjà été gardé");
        return (plante.getGardes() != null) && !(plante.getGardes().isEmpty());
    }

    private boolean isOneOrMorePhotos(Plante plante) {
        log.info("Vérification si la plante a au moins une photo");
        return plante.getPhotos() != null && !plante.getPhotos().isEmpty();
    }
}
