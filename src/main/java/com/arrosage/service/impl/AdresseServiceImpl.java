package com.arrosage.service.impl;

import com.arrosage.exceptions.AdresseException;
import com.arrosage.model.Adresse;
import com.arrosage.repository.AdresseRepository;
import com.arrosage.service.AdresseService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class AdresseServiceImpl implements AdresseService {
    private final AdresseRepository adresseRepository;

    public AdresseServiceImpl(AdresseRepository adresseRepository) {
        this.adresseRepository = adresseRepository;
    }

    /**
     * @param id
     * @return
     * @throws AdresseException
     */
    @Override
    public Adresse getById(Integer id) throws AdresseException {
        try {
            return this.adresseRepository.getReferenceById(id);
        } catch (EntityNotFoundException e) {
            throw new AdresseException("cette adresse est inconnue");
        }

    }

    /**
     * @param adresse
     * @return the Adresse
     * @throws AdresseException
     */
    @Override
    public Adresse createAdresse(Adresse adresse) throws AdresseException {
        Adresse adresse1 = null;
        if ((adresse.getLatitude() != null) && (!adresse.getLatitude().isNaN())) {
            if ((adresse.getLongitude() != null) && (!adresse.getLongitude().isNaN())) {
                adresse1 = this.adresseRepository.save(adresse);
            }
        } else {
            throw new AdresseException("l'adresse n'est pas au bon format");
        }
        return adresse1;
    }

    @Override
    public Adresse updateAdresse(Adresse adresse) {
        return null;
    }
}
