package com.arrosage.service.impl;

import com.arrosage.exceptions.MesageNotFoundException;
import com.arrosage.model.Message;
import com.arrosage.repository.MessageRepository;
import com.arrosage.service.MessageService;
import com.arrosage.service.UtilisateurService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;
    private final UtilisateurService utilisateurService;

    public MessageServiceImpl(MessageRepository messageRepository, UtilisateurService utilisateurService) {
        this.messageRepository = messageRepository;
        this.utilisateurService = utilisateurService;
    }

    @Override
    public List<Message> getAllMessageByEmeteurAndRecepteur(int emeteur, int recepteur) {
        List<Message> chat = messageRepository.findAllMessageByEmeteurAndRecepteur(emeteur, recepteur);
        return chat;
    }

    @Override
    public Message getById(Integer id) {
        return this.messageRepository.getReferenceById(id);
    }

    @Override
    public Message createMessage(Message message) throws MesageNotFoundException{
        if (message.getBody() != null && utilisateurService.getUserById(message.getEmeteur().getId()) != null
                && utilisateurService.getUserById(message.getRecepteur().getId()) != null) {
            return this.messageRepository.save(message);
        } else {
            throw new MesageNotFoundException();
        }
    }

}
