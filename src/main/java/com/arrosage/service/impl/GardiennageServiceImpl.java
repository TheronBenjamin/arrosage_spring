package com.arrosage.service.impl;

import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.UtilisateurException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageDateException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageProprioException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageUnknownException;
import com.arrosage.model.Gardiennage;
import com.arrosage.model.Plante;
import com.arrosage.model.Utilisateur;
import com.arrosage.repository.GardiennageRepository;
import com.arrosage.repository.PlanteRepository;
import com.arrosage.repository.UtilisateurRepository;
import com.arrosage.service.GardiennageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class GardiennageServiceImpl implements GardiennageService {
    Logger log = LoggerFactory.getLogger(GardiennageServiceImpl.class);
    private final GardiennageRepository gardiennageRepository;
    private final UtilisateurRepository utilisateurRepository;
    private final PlanteRepository planteRepository;

    public GardiennageServiceImpl(GardiennageRepository gardiennageRepository, UtilisateurRepository utilisateurRepository, PlanteRepository planteRepository) {
        this.gardiennageRepository = gardiennageRepository;
        this.utilisateurRepository = utilisateurRepository;
        this.planteRepository = planteRepository;
    }

    /**
     * Permet de créer un gardiennage, nécessite une date de début, un propriétaire et une plante
     *
     * @param gardiennage
     * @return Gardiennage
     * @throws GardiennageDateException
     * @throws GardiennageProprioException
     * @throws PlanteUnknownException
     */
    @Override
    public Gardiennage createGardiennage(Gardiennage gardiennage) throws GardiennageDateException, GardiennageProprioException, PlanteUnknownException {
        Gardiennage gardiennageToAdd = new Gardiennage();
        log.info("attempting to create new gardiennage");
        if (gardiennage.getDateDebut() != null) {
            gardiennageToAdd.setDateDebut(gardiennage.getDateDebut());
        } else {
            log.error("Pas de date de début");
            throw new GardiennageDateException();
        }

        if (gardiennage.getProprietaire() != null) {
            Utilisateur proprietaire = this.getProprietairePlante(gardiennage.getProprietaire());
            gardiennageToAdd.setProprietaire(proprietaire);
        } else {
            log.error("Pas de propriétaire associé");
            throw new GardiennageProprioException();
        }

        if (gardiennage.getPlante() != null) {
            Plante plante = this.getGardiennagePlante(gardiennage.getPlante());
            gardiennageToAdd.setPlante(plante);
        } else {
            log.error("Pas de plante associée");
            throw new PlanteUnknownException("Ce gardiennage n'est pas associée à une plante");
        }

        gardiennageToAdd.setGardien(null);
        gardiennageToAdd.setDateFin(null);
        log.info("attempting to save new gardiennage");
        return this.gardiennageRepository.save(gardiennageToAdd);
    }

    /**
     * Permet de modifier la date de fin du gardiennage, celle-ci ne doit pas être antérieure à la date de début
     *
     * @param gardiennage
     * @return Gardiennage avec une date de fin
     * @throws GardiennageDateException
     */
    @Override
    public Gardiennage updateGardiennage(Gardiennage gardiennage) throws GardiennageDateException {
        Gardiennage gardiennageToUpdate = this.gardiennageRepository.getReferenceById(gardiennage.getId());

        if (gardiennage.getDateFin() != null && gardiennage.getDateFin().after(gardiennageToUpdate.getDateDebut())) {
            gardiennageToUpdate.setDateFin(gardiennage.getDateFin());
        } else {
            log.info("date fin antérieure à date début");
            throw new GardiennageDateException("la date de fin n'est pas valide");
        }

        return gardiennageToUpdate;
    }

    //TODO: Do the same for other services
    @Override
    public Gardiennage getById(Integer id) throws GardiennageUnknownException {
        try {
            return this.gardiennageRepository.getReferenceById(id);
        } catch (EntityNotFoundException e) {
            throw new GardiennageUnknownException();
        }
    }

    @Override
    public List<Gardiennage> getAll() {
        return this.gardiennageRepository.findAll();
    }

    public List<Gardiennage> getGardiennagesByPlante(Plante plante) {
        return this.gardiennageRepository.findGardiennagesByPlanteId(plante.getId());
    }

    public List<Gardiennage> getGardiennagesByProprietaire(Utilisateur utilisateur) {
        return this.gardiennageRepository.findGardiennagesByProprietaireId(utilisateur.getId());
    }

    public List<Gardiennage> getGardiennagesByGardien(Utilisateur utilisateur) {
        return this.gardiennageRepository.findGardiennagesByGardienId(utilisateur.getId());
    }

    private Utilisateur getProprietairePlante(Utilisateur proprietaire) {
        log.info("recherche du propriétaire associée au gardiennage");
        try {
            return this.utilisateurRepository.getReferenceById(proprietaire.getId());
        } catch (EntityNotFoundException e) {
            throw new UtilisateurException("utilisateur inconnu");
        }
    }

    private Plante getGardiennagePlante(Plante plante) throws PlanteUnknownException {
        log.info("recherche de la plante associée au gardiennage");
        try {
            return this.planteRepository.getReferenceById(plante.getId());
        } catch (EntityNotFoundException e) {
            throw new PlanteUnknownException("plante inconnu");
        }
    }
}
