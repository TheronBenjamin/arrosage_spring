package com.arrosage.service.impl;

import com.arrosage.exceptions.conseil.exception.ConseilAvisException;
import com.arrosage.exceptions.conseil.exception.ConseilRuntimeException;
import com.arrosage.exceptions.conseil.exception.ConseilUnknownException;
import com.arrosage.model.Botaniste;
import com.arrosage.model.Conseil;
import com.arrosage.model.Photo;
import com.arrosage.repository.ConseilRepository;
import com.arrosage.service.ConseilService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ConseilServiceImpl implements ConseilService {
    Logger log = LoggerFactory.getLogger(ConseilServiceImpl.class);
    private final ConseilRepository conseilRepository;

    public ConseilServiceImpl(ConseilRepository conseilRepository) {
        this.conseilRepository = conseilRepository;
    }

    @Override
    public List<Conseil> getAll() {
        return this.conseilRepository.findAll();
    }

    @Override
    public Conseil getById(Integer id) throws ConseilUnknownException {
        try {
            return this.conseilRepository.getReferenceById(id);
        } catch (EntityNotFoundException e) {
            throw new ConseilUnknownException();
        }

    }

    /**
     * Permet de créer un conseil
     * //TODO: vérifier la longueur du conseil?
     *
     * @param conseil
     * @return
     * @throws ConseilAvisException
     */
    @Override
    public Conseil createConseil(Conseil conseil) throws ConseilAvisException {
        if (conseil.getAvis() != null && conseil.getBotaniste() != null && conseil.getPhoto() != null) {
            return this.conseilRepository.save(conseil);
        } else {
            throw new ConseilAvisException();
        }
    }


    /**
     * Permet de modifier le contenu d'un conseil
     *
     * @param conseil
     * @return Conseil
     * @throws ConseilAvisException
     */
    @Override
    public Conseil updateConseil(Conseil conseil) throws ConseilAvisException {
        try {
            Conseil conseil1 = this.getById(conseil.getId());
            if (conseil.getAvis() != null) {
                conseil1.setAvis(conseil.getAvis());
                conseil1 = this.conseilRepository.save(conseil1);
                return conseil1;
            } else {
                throw new ConseilAvisException("il n'y a pas de message");
            }
        } catch (ConseilUnknownException e) {
            log.error("update de ce conseil impossible, il n'est pas connu");
            throw new ConseilRuntimeException(e.getMessage());
        }
    }

    /**
     * @param botaniste
     * @return une liste de conseils écrit par un botaniste
     */
    public List<Conseil> getConseilsByBotaniste(Botaniste botaniste) {
        return this.conseilRepository.findConseilsByBotanisteId(botaniste.getId());
    }

    /**
     * @param photo
     * @return une liste de conseils associé à une photo
     */
    public List<Conseil> getConseilsByPhoto(Photo photo) {
        return this.conseilRepository.findConseilsByPhotoId(photo.getId());
    }
}
