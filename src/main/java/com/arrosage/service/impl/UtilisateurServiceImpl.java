package com.arrosage.service.impl;

import com.arrosage.config.JwtTokenProvider;
import com.arrosage.config.MyUserDetails;
import com.arrosage.dto.AuthenticationRequest;
import com.arrosage.dto.AuthenticationResponse;
import com.arrosage.exceptions.UtilisateurException;
import com.arrosage.model.Role;
import com.arrosage.model.Utilisateur;
import com.arrosage.repository.UtilisateurRepository;
import com.arrosage.service.GardiennageService;
import com.arrosage.service.PhotoService;
import com.arrosage.service.UtilisateurService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UtilisateurServiceImpl implements UtilisateurService {
    Logger log = LoggerFactory.getLogger(UtilisateurServiceImpl.class);

    private final UtilisateurRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtService;
    private final AuthenticationManager authenticationManager;

    private final GardiennageService gardiennageService;
    private final PhotoService photoService;

    @Override
    public List<Utilisateur> getAllUsers() {
        return this.userRepository.findAll();
    }

    @Override
    public Utilisateur getUserById(Integer id) {
        return this.userRepository.findById(id).
                orElseThrow(() -> new UtilisateurException("User not found by the given id"));
    }

    @Override
    public Utilisateur getUserByMailAndPassword(String mail, String password) {
        Utilisateur user = this.getUserByMail(mail);
        if (passwordEncoder.matches(password, user.getPassword())) {
            return user;
        }
        throw new UtilisateurException("No user found by the given mail/password.");
    }

    @Override
    public Utilisateur getUserByMail(String mail) {
        return this.userRepository.findByMail(mail).
                orElseThrow(() -> new UtilisateurException("No user found by the given mail"));
    }

    /**
     * @param user
     * @return Utilisateur
     */
    @Override
    public Utilisateur updateUser(Utilisateur user) {
        Utilisateur userToUpdate = this.getUserById(user.getId());
        userToUpdate.setMail(user.getMail());
        userToUpdate.setLogin(user.getLogin());

        if (user.getGardiennagesGardien() != null && !user.getGardiennagesGardien().isEmpty()) {
            userToUpdate.setGardiennagesGardien(this.gardiennageService.getGardiennagesByGardien(user));
        } else {
            userToUpdate.setGardiennagesGardien(null);
        }

        if (user.getGardiennagesProprio() != null && !user.getGardiennagesProprio().isEmpty()) {
            userToUpdate.setGardiennagesProprio(this.gardiennageService.getGardiennagesByProprietaire(user));
        } else {
            userToUpdate.setGardiennagesProprio(null);
        }

        if (user.getPhotos() != null && !user.getPhotos().isEmpty()) {
            userToUpdate.setPhotos(this.photoService.getPhotosByUtilisateur(user));
        } else {
            userToUpdate.setPhotos(null);
        }

        userToUpdate.setRole(user.getRole());
//        TODO réaliser la vérification de la longueur du Password
        String passwordEncoded = passwordEncoder.encode(user.getPassword());
        userToUpdate.setPassword(passwordEncoded);
        return this.userRepository.save(userToUpdate);
    }

    @Override
    public void deleteUser(Integer id) {
        Utilisateur userToDelete = this.getUserById(id);
        userRepository.delete(userToDelete);
    }

    /**
     * Méthode permettant de créer un utilisateur
     *
     * @param utilisateur
     * @return AuthenticationResponse
     */
    @Override
    public AuthenticationResponse register(Utilisateur utilisateur) {
        if (this.userRepository.findByMail(utilisateur.getMail()).isPresent()) {
            throw new UtilisateurException("User already exists");
        } else {
            var user = Utilisateur.builder()
                    .mail(utilisateur.getMail())
                    .login(utilisateur.getLogin())
                    .password(passwordEncoder.encode(utilisateur.getPassword()))
                    .photos(null)
                    .gardiennagesProprio(null)
                    .gardiennagesGardien(null)
                    .role(Role.ROLE_USER)
                    .build();
            userRepository.save(user);
            var jwtToken = jwtService.generateToken(new MyUserDetails(user));
            return AuthenticationResponse
                    .builder()
                    .token(jwtToken)
                    .build();
        }
    }

    /**
     * Méthode permettant l'authentification de l'utilisateur
     *
     * @param authenticationRequest
     * @return AuthenticationResponse
     */
    @Override
    public AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getMail(),
                        authenticationRequest.getPassword()
                )
        );
        var user = userRepository.findByMail(authenticationRequest.getMail()).orElseThrow();
        var jwtToken = jwtService.generateToken(new MyUserDetails(user));
        return AuthenticationResponse
                .builder()
                .token(jwtToken)
                .build();
    }


}
