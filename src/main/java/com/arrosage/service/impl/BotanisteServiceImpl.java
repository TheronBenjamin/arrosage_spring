package com.arrosage.service.impl;


import com.arrosage.exceptions.conseil.exception.ConseilRuntimeException;
import com.arrosage.model.Botaniste;
import com.arrosage.model.Conseil;
import com.arrosage.repository.BotanisteRepository;
import com.arrosage.service.BotanisteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BotanisteServiceImpl implements BotanisteService {
    Logger log = LoggerFactory.getLogger(BotanisteServiceImpl.class);

    private final BotanisteRepository botanisteRepository;
    private final ConseilServiceImpl conseilService;

    public BotanisteServiceImpl(BotanisteRepository botanisteRepository, ConseilServiceImpl conseilService) {
        this.botanisteRepository = botanisteRepository;
        this.conseilService = conseilService;
    }

    /**
     * @return a list of all botaniste
     */
    @Override
    public List<Botaniste> getAll() {
        return this.botanisteRepository.findAll();
    }

    /**
     * @param id
     * @return a botaniste get by id
     */
    @Override
    public Botaniste getById(Integer id) {
        return this.botanisteRepository.getReferenceById(id);
    }

    /**
     * @param botaniste
     * @return
     */
    @Override
    public Botaniste createBotaniste(Botaniste botaniste) {
        log.info("Attempting to create botaniste");
        return this.botanisteRepository.save(botaniste);
    }

    /**
     * Permet de modifier la spécialité du botaniste
     *
     * @param botaniste
     * @return
     */
    @Override
    public Botaniste updateBotaniste(Botaniste botaniste) {
        log.info("Attempting to update botaniste");
        Botaniste botanisteRef = botanisteRepository.getReferenceById(botaniste.getId());
        if (botaniste.getSpecialite() != null) {
            botanisteRef.setSpecialite(botaniste.getSpecialite());
        }
        List<Conseil> conseils = this.getConseils(botaniste);
        botanisteRef.setConseils(conseils);
        return this.botanisteRepository.save(botanisteRef);
    }

    /**
     * @param botaniste
     * @return a list of conseil
     */
    private List<Conseil> getConseils(Botaniste botaniste) {
        try {
            return this.conseilService.getConseilsByBotaniste(botaniste);
        } catch (ConseilRuntimeException e) {
            throw new ConseilRuntimeException(e.getMessage());
        }
    }
}
