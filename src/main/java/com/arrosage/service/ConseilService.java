package com.arrosage.service;

import com.arrosage.exceptions.conseil.exception.ConseilAvisException;
import com.arrosage.exceptions.conseil.exception.ConseilUnknownException;
import com.arrosage.model.Botaniste;
import com.arrosage.model.Conseil;
import com.arrosage.model.Photo;

import java.util.List;

public interface ConseilService {
    List<Conseil> getAll();

    Conseil getById(Integer id) throws ConseilUnknownException;

    Conseil createConseil(Conseil conseil) throws ConseilAvisException;

    Conseil updateConseil(Conseil conseil) throws ConseilAvisException;

    List<Conseil> getConseilsByBotaniste(Botaniste botaniste);

    List<Conseil> getConseilsByPhoto(Photo photo);
}
