package com.arrosage.service;

import com.arrosage.model.Message;

import java.util.List;

public interface MessageService {

    List<Message> getAllMessageByEmeteurAndRecepteur(int emeteur, int recepteur);

    Message getById(Integer id);

    Message createMessage(Message message);
}
