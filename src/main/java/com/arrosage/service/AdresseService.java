package com.arrosage.service;

import com.arrosage.exceptions.AdresseException;
import com.arrosage.model.Adresse;

public interface AdresseService {

    Adresse getById(Integer id) throws AdresseException;

    Adresse createAdresse(Adresse adresse) throws AdresseException;

    Adresse updateAdresse(Adresse adresse);
}
