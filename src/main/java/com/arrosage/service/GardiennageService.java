package com.arrosage.service;

import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageDateException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageProprioException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageUnknownException;
import com.arrosage.model.Gardiennage;
import com.arrosage.model.Plante;
import com.arrosage.model.Utilisateur;

import java.util.List;

public interface GardiennageService {

    Gardiennage createGardiennage(Gardiennage gardiennage) throws GardiennageDateException, GardiennageProprioException, PlanteUnknownException;

    Gardiennage updateGardiennage(Gardiennage gardiennage) throws GardiennageDateException;

    Gardiennage getById(Integer id) throws GardiennageUnknownException;

    List<Gardiennage> getAll();

    List<Gardiennage> getGardiennagesByPlante(Plante plante);

    List<Gardiennage> getGardiennagesByGardien(Utilisateur utilisateur);

    List<Gardiennage> getGardiennagesByProprietaire(Utilisateur utilisateur);
}
