package com.arrosage.service;

import com.arrosage.exceptions.AdresseException;
import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.photo.exception.PhotoEmptyException;
import com.arrosage.model.Plante;

import java.util.List;

public interface PlanteService {
    List<Plante> getAll();

    Plante getById(Integer id) throws PlanteUnknownException;

    Plante createPlante(Plante plante) throws AdresseException;

    Plante updatePlante(Plante plante) throws PhotoEmptyException, AdresseException, PlanteUnknownException;
}
