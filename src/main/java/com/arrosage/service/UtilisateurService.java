package com.arrosage.service;

import com.arrosage.dto.AuthenticationRequest;
import com.arrosage.dto.AuthenticationResponse;
import com.arrosage.model.Utilisateur;

import java.util.List;

public interface UtilisateurService {

    List<Utilisateur> getAllUsers();
    Utilisateur getUserById(Integer id);
    Utilisateur getUserByMailAndPassword(String mail, String password);
    Utilisateur getUserByMail(String mail);
    AuthenticationResponse register(Utilisateur user);
    AuthenticationResponse authenticate(AuthenticationRequest authenticationRequest);
    Utilisateur updateUser(Utilisateur user);
    void deleteUser(Integer id);

}
