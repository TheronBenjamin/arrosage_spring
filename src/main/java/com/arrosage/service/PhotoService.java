package com.arrosage.service;

import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.UtilisateurException;
import com.arrosage.exceptions.photo.exception.PhotoDateException;
import com.arrosage.model.Photo;
import com.arrosage.model.Plante;
import com.arrosage.model.Utilisateur;

import java.util.List;

public interface PhotoService {
    List<Photo> getAll();

    Photo getById(Integer id);

    Photo createPhoto(Photo photo) throws PhotoDateException, PlanteUnknownException, UtilisateurException;

    Photo updatePhoto(Photo photo) throws PhotoDateException, PlanteUnknownException, UtilisateurException;

    List<Photo> getPhotosByUtilisateur(Utilisateur utilisateur);

    List<Photo> getPhotosByPlante(Plante plante);
}
