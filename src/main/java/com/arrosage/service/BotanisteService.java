package com.arrosage.service;

import com.arrosage.model.Botaniste;

import java.util.List;

public interface BotanisteService {

    List<Botaniste> getAll();

    Botaniste getById(Integer id);

    Botaniste createBotaniste(Botaniste botaniste);

    Botaniste updateBotaniste(Botaniste botaniste);
}
