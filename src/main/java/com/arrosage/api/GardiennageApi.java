package com.arrosage.api;

import com.arrosage.dto.GardiennageDto;
import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageDateException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageProprioException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageUnknownException;
import com.arrosage.mapping.GardiennageMapper;
import com.arrosage.service.GardiennageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/gardiennages")
public class GardiennageApi {

    private final GardiennageService gardiennageService;
    private final GardiennageMapper gardiennageMapper;

    public GardiennageApi(GardiennageService gardiennageService, GardiennageMapper gardiennageMapper) {
        this.gardiennageService = gardiennageService;
        this.gardiennageMapper = gardiennageMapper;
    }

    //CREATE
    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create a gardiennage")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = " Bad Request")
    public ResponseEntity<GardiennageDto> createGardiennage(@RequestBody final GardiennageDto gardiennageDto) {
        try {
            GardiennageDto gardiennageDtoResponse = this.gardiennageMapper.mapToDto(
                    this.gardiennageService.createGardiennage(
                            this.gardiennageMapper.mapToModel(gardiennageDto)
                    ));
            return ResponseEntity
                    .created(URI.create("/gardiennages/" + gardiennageDto.getId()))
                    .body(gardiennageDtoResponse);
        } catch (GardiennageDateException | GardiennageProprioException | PlanteUnknownException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //UPDATE
    @PutMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "updateGardiennage")
    @ApiResponse(responseCode = "200", description = " Update")
    @ApiResponse(responseCode = "400", description = " Bad Request")
    public ResponseEntity<GardiennageDto> updateGardiennage(@RequestBody GardiennageDto gardiennageDto, @PathVariable Integer id) {
        try {
            gardiennageDto.setId(id);
            GardiennageDto gardiennageDtoReponse = gardiennageMapper.mapToDto(gardiennageService.updateGardiennage(
                    gardiennageMapper.mapToModel(gardiennageDto)));
            return ResponseEntity.ok(gardiennageDtoReponse);
        } catch (GardiennageDateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //GET BY ID
    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return a gardiennage")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "404", description = "Gardiennage not found")
    public ResponseEntity<GardiennageDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.gardiennageMapper
                    .mapToDto(this.gardiennageService.getById(id)));
        } catch (GardiennageUnknownException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    //GET ALL
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return a list of gardiennage")
    public ResponseEntity<List<GardiennageDto>> getAll() {
        return ResponseEntity.ok(
                this.gardiennageService.getAll().stream()
                        .map(this.gardiennageMapper::mapToDto)
                        .toList()
        );
    }
}
