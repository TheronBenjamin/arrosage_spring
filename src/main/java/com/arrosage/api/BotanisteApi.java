package com.arrosage.api;

import com.arrosage.dto.BotanisteDto;
import com.arrosage.mapping.BotanisteMapper;
import com.arrosage.service.BotanisteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/botanistes")
public class BotanisteApi {

    private final BotanisteService botanisteService;
    private final BotanisteMapper botanisteMapper;

    public BotanisteApi(BotanisteService botanisteService, BotanisteMapper botanisteMapper) {
        this.botanisteService = botanisteService;
        this.botanisteMapper = botanisteMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of botanistes ")
    public ResponseEntity<List<BotanisteDto>> getAll() {

        return ResponseEntity.ok(
                this.botanisteService.getAll().stream()
                        .map(this.botanisteMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}",produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return a botaniste")
    @ApiResponse(responseCode = "200", description = "ok")
    @ApiResponse(responseCode = "400", description = " Unknown botaniste")
    public ResponseEntity<BotanisteDto> getById(@PathVariable Integer id ) {
        try {
            return ResponseEntity.ok(this.botanisteMapper
                    .mapToDto(this.botanisteService.getById(id)));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create a botaniste")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<BotanisteDto> createBotaniste(@RequestBody final BotanisteDto botanisteDto) {

        try{
            BotanisteDto botanisteDtoResponse =
                    this.botanisteMapper.mapToDto(
                            this.botanisteService.createBotaniste(
                                    this.botanisteMapper.mapToModel(botanisteDto)
                            ));
            return ResponseEntity
                    .created(URI.create("/botanistes/" + botanisteDto.getId()))
                    .body(botanisteDtoResponse);
            //TODO: Modifier Exception.
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE} )
    @Operation(summary = "updateBotaniste")
    public ResponseEntity<BotanisteDto> updateBotaniste(@RequestBody BotanisteDto botanisteDto, @PathVariable Integer id){
        try{
            botanisteDto.setId(id);
            BotanisteDto botanisteDtoResponse= botanisteMapper.mapToDto(botanisteService.updateBotaniste(
                    botanisteMapper.mapToModel(botanisteDto)));
            return ResponseEntity.ok(botanisteDtoResponse);
            //TODO: Modifier Exception.
        }catch (UnknownError e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
