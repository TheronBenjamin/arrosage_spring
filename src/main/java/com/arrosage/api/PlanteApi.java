package com.arrosage.api;

import com.arrosage.dto.PlanteDto;
import com.arrosage.exceptions.AdresseException;
import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.photo.exception.PhotoEmptyException;
import com.arrosage.mapping.PlanteMapper;
import com.arrosage.service.PlanteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/plantes")
public class PlanteApi {

    private final PlanteService planteService;
    private final PlanteMapper planteMapper;

    public PlanteApi(PlanteService planteService, PlanteMapper planteMapper) {
        this.planteService = planteService;
        this.planteMapper = planteMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of plantes ")
    public ResponseEntity<List<PlanteDto>> getAll() {

        return ResponseEntity.ok(
                this.planteService.getAll().stream()
                        .map(this.planteMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}",produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return a plante")
    @ApiResponse(responseCode = "200", description = "ok")
    @ApiResponse(responseCode = "404", description = " Unknown plante")
    public ResponseEntity<PlanteDto> getById(@PathVariable Integer id ) {
        try {
            return ResponseEntity.ok(this.planteMapper
                    .mapToDto(this.planteService.getById(id)));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create a plante")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<PlanteDto> createPlante(@RequestBody final PlanteDto planteDto) {

        try {
            PlanteDto planteDtoResponse =
                    this.planteMapper.mapToDto(
                            this.planteService.createPlante(
                                    this.planteMapper.mapToModel(planteDto)
                            ));
            return ResponseEntity
                    .created(URI.create("/plantes/" + planteDto.getId()))
                    .body(planteDtoResponse);
        } catch (AdresseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "updatePlante")
    @ApiResponse(responseCode = "200", description = "Update")
    @ApiResponse(responseCode = "400", description = " bad request")
    public ResponseEntity<PlanteDto> updatePlante(@RequestBody PlanteDto planteDto, @PathVariable Integer id){
        try {
            planteDto.setId(id);
            PlanteDto planteDtoResponse = planteMapper.mapToDto(planteService.updatePlante(
                    planteMapper.mapToModel(planteDto)));
            return ResponseEntity.ok(planteDtoResponse);
        } catch (PhotoEmptyException | AdresseException | PlanteUnknownException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
