package com.arrosage.api;

import com.arrosage.dto.PhotoDto;
import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.UtilisateurException;
import com.arrosage.exceptions.photo.exception.PhotoDateException;
import com.arrosage.mapping.PhotoMapper;
import com.arrosage.service.PhotoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/api/photos")
public class PhotoApi {
    private final PhotoService photoService;
    private final PhotoMapper photoMapper;

    public PhotoApi(PhotoService photoService, PhotoMapper photoMapper) {
        this.photoService = photoService;
        this.photoMapper = photoMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of photos ")
    @ApiResponse(responseCode = "200", description = "ok")
    @ApiResponse(responseCode = "404", description = " Not Found")
    public ResponseEntity<List<PhotoDto>> getAll() {

        return ResponseEntity.ok(
                this.photoService.getAll().stream()
                        .map(this.photoMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return a photo")
    @ApiResponse(responseCode = "200", description = "ok")
    @ApiResponse(responseCode = "404", description = " Unknown photo")
    public ResponseEntity<PhotoDto> getById(@PathVariable Integer id ) {
        try {
            return ResponseEntity.ok(this.photoMapper
                    .mapToDto(this.photoService.getById(id)));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create a photo")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = " bad request")
    public ResponseEntity<PhotoDto> createPhoto(@RequestParam("file") MultipartFile file,
                                                @RequestParam("descriptif") String descriptif,
                                                @RequestParam("planteId") Integer planteId,
                                                @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date,
                                                @RequestParam("photographeId") Integer photographeId) throws IOException {

        PhotoDto photoDto = new PhotoDto();

        photoDto.setPlanteId(planteId);
        photoDto.setDescriptif(descriptif);
        photoDto.setPhotographeId(photographeId);
        photoDto.setDatePrisePhoto(date);


        if (file != null) {
            // Read the content of the Blob object
            byte[] photoData = file.getBytes();
            photoDto.setImage(photoData);
        }


        try {
            PhotoDto photoDtoResponse =
                    this.photoMapper.mapToDto(
                            this.photoService.createPhoto(
                                    this.photoMapper.mapToModel(photoDto)
                            ));

            return ResponseEntity
                    .created(URI.create("/photos/" + photoDto.getId()))
                    .body(photoDtoResponse);

        } catch (PhotoDateException | PlanteUnknownException | UtilisateurException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "updatePhoto")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = " bad request")
    public ResponseEntity<PhotoDto> updatePhoto(@RequestBody PhotoDto photoDto, @PathVariable Integer id){
        try {
            photoDto.setId(id);
            PhotoDto photoDtoResponse = photoMapper.mapToDto(photoService.updatePhoto(
                    photoMapper.mapToModel(photoDto)));
            return ResponseEntity.ok(photoDtoResponse);
        } catch (PhotoDateException | PlanteUnknownException | UtilisateurException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
