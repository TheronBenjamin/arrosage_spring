package com.arrosage.api;

import com.arrosage.dto.AuthenticationRequest;
import com.arrosage.dto.AuthenticationResponse;
import com.arrosage.dto.UtilisateurDto;
import com.arrosage.exceptions.UtilisateurException;
import com.arrosage.mapping.UtilisateurMapper;
import com.arrosage.service.UtilisateurService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserApi {

    private final UtilisateurService utilisateurService;
    private final UtilisateurMapper utilisateurMapper;

    public UserApi(UtilisateurService utilisateurService, UtilisateurMapper utilisateurMapper) {
        this.utilisateurService = utilisateurService;
        this.utilisateurMapper = utilisateurMapper;
    }


    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(
            @RequestBody UtilisateurDto utilisateurDto
    ) {
        return ResponseEntity.ok(utilisateurService.register(utilisateurMapper.mapToModel(utilisateurDto)));
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> register(
            @RequestBody AuthenticationRequest request
    ) {
        return ResponseEntity.ok(utilisateurService.authenticate(request));
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    public ResponseEntity<UtilisateurDto> update(
            @RequestBody final UtilisateurDto utilisateurDto,
            @PathVariable final Integer id
    ) {
        utilisateurDto.setId(id);
        this.utilisateurService.updateUser(utilisateurMapper.mapToModel(utilisateurDto));
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/{mail}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UtilisateurDto> getUserByMail(@PathVariable final String mail) {
        try {
            return ResponseEntity.ok(this.utilisateurMapper.mapToDto(this.utilisateurService.getUserByMail(mail)));
        } catch (UtilisateurException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<UtilisateurDto>> getAllUsers() {
        try {
            return ResponseEntity.ok(
                    this.utilisateurService.getAllUsers().stream()
                            .map(this.utilisateurMapper::mapToDto)
                            .toList()
            );
        } catch (UtilisateurException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @GetMapping(value = "/getUserById/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UtilisateurDto> getUserById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.utilisateurMapper.mapToDto(this.utilisateurService.getUserById(id)));
        } catch (UtilisateurException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

}
