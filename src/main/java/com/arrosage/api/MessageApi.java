package com.arrosage.api;


import com.arrosage.dto.MessageDto;
import com.arrosage.exceptions.MesageNotFoundException;
import com.arrosage.mapping.MessageMapper;
import com.arrosage.service.MessageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/api/messages")
public class MessageApi {

    private final MessageService messageService;

    private final MessageMapper messageMapper;

    public MessageApi(MessageService messageService, MessageMapper messageMapper) {
        this.messageService = messageService;
        this.messageMapper = messageMapper;
    }

    @GetMapping(value = "getMessageByEmeteurAndRecepteur/{emeteur}/{recepteur}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<MessageDto>> getAllMessageByEmeteurAndRecepteur(@PathVariable int emeteur, @PathVariable int recepteur ) {

        try {
            return ResponseEntity.ok(this.messageService.getAllMessageByEmeteurAndRecepteur(emeteur, recepteur)
                            .stream()
                            .map(this.messageMapper::mapToDto)
                            .toList()
            );
        } catch (MesageNotFoundException e) {
            throw new MesageNotFoundException(e.getMessage());
        }
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return a message")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "404", description = "message not found")
    public ResponseEntity<MessageDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.messageMapper
                    .mapToDto(this.messageService.getById(id)));
        } catch (MesageNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE}
    )
    @Operation(summary = "Create a Message")
    @ApiResponse(responseCode = "201", description = "Created")
    @ApiResponse(responseCode = "400", description = " Bad Request")
    public ResponseEntity<MessageDto> createMessage(@RequestBody final MessageDto messageDto) {
        try {
            MessageDto response = this.messageMapper.mapToDto(
                    this.messageService.createMessage(
                            this.messageMapper.mapToModel(messageDto)
                    ));
            return ResponseEntity
                    .created(URI.create("/messages/" + messageDto.getId()))
                    .body(response);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
