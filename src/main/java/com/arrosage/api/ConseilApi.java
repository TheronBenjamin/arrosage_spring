package com.arrosage.api;

import com.arrosage.dto.ConseilDto;
import com.arrosage.exceptions.conseil.exception.ConseilAvisException;
import com.arrosage.exceptions.conseil.exception.ConseilUnknownException;
import com.arrosage.mapping.ConseilMapper;
import com.arrosage.service.ConseilService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/conseils")
public class ConseilApi {

    private final ConseilService conseilService;
    private final ConseilMapper conseilMapper;

    public ConseilApi(ConseilService conseilService, ConseilMapper conseilMapper) {
        this.conseilService = conseilService;
        this.conseilMapper = conseilMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of conseils ")
    public ResponseEntity<List<ConseilDto>> getAll() {

        return ResponseEntity.ok(
                this.conseilService.getAll().stream()
                        .map(this.conseilMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return a conseil")
    @ApiResponse(responseCode = "200", description = "ok")
    @ApiResponse(responseCode = "404", description = " Unknown conseil")
    public ResponseEntity<ConseilDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.conseilMapper
                    .mapToDto(this.conseilService.getById(id)));
        } catch (ConseilUnknownException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create a conseil")
    @ApiResponse(responseCode = "201", description = "Created")
    public ResponseEntity<ConseilDto> createConseil(@RequestBody final ConseilDto conseilDto) {

        try {
            ConseilDto conseilDtoResponse =
                    this.conseilMapper.mapToDto(
                            this.conseilService.createConseil(
                                    this.conseilMapper.mapToModel(conseilDto)
                            ));
            return ResponseEntity
                    .created(URI.create("/conseils/" + conseilDto.getId()))
                    .body(conseilDtoResponse);
        } catch (ConseilAvisException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "updateConseil")
    public ResponseEntity<ConseilDto> updateConseil(@RequestBody ConseilDto conseilDto, @PathVariable Integer id) {
        try {
            conseilDto.setId(id);
            ConseilDto conseilDtoResponse = conseilMapper.mapToDto(conseilService.updateConseil(
                    conseilMapper.mapToModel(conseilDto)));
            return ResponseEntity.ok(conseilDtoResponse);
        } catch (ConseilAvisException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
