package com.arrosage.repository;

import com.arrosage.model.Conseil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConseilRepository extends JpaRepository<Conseil, Integer> {

    List<Conseil> findConseilsByBotanisteId(Integer id);

    List<Conseil> findConseilsByPhotoId(Integer id);
}
