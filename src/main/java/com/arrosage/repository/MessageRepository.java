package com.arrosage.repository;

import com.arrosage.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface MessageRepository extends JpaRepository<Message, Integer> {

    @Query(value = "SELECT * FROM message m WHERE m.emeteur = :emeteur AND m.recepteur= :recepteur OR (m.emeteur = :recepteur AND m.recepteur= :emeteur) ORDER BY m.date_envoi", nativeQuery = true)
    List<Message> findAllMessageByEmeteurAndRecepteur(@Param("emeteur") int emeteur, @Param("recepteur") int recepteur);
}
