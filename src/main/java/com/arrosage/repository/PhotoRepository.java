package com.arrosage.repository;

import com.arrosage.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Integer> {
    List<Photo> findPhotosByPhotographeId(Integer id);

    List<Photo> findPhotosByPlanteId(Integer id);
}
