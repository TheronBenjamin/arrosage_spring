package com.arrosage.repository;

import com.arrosage.model.Gardiennage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GardiennageRepository extends JpaRepository<Gardiennage, Integer> {
    List<Gardiennage> findGardiennagesByPlanteId(Integer id);

    List<Gardiennage> findGardiennagesByProprietaireId(Integer id);

    List<Gardiennage> findGardiennagesByGardienId(Integer id);


}
