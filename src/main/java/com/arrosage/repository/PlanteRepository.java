package com.arrosage.repository;

import com.arrosage.model.Plante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanteRepository extends JpaRepository<Plante, Integer> {

    
}
