package com.arrosage.repository;

import com.arrosage.model.Botaniste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BotanisteRepository extends JpaRepository<Botaniste, Integer> {
}
