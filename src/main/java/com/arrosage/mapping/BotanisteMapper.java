package com.arrosage.mapping;

import com.arrosage.dto.BotanisteDto;
import com.arrosage.model.Botaniste;
import com.arrosage.model.Gardiennage;
import com.arrosage.model.Photo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface BotanisteMapper {

    @Mapping(target = "password", ignore = true)
    @Mapping(target = "gardiennageProprietaireIds", expression = "java(gardiennageProprietaireIds(botaniste))")
    @Mapping(target = "gardiennageGardienIds", expression = "java(gardiennageGardienIds(botaniste))")
    @Mapping(target = "photosIds", expression = "java(getPhotosId(botaniste))")
    BotanisteDto mapToDto(Botaniste botaniste);


    default List<Integer> getPhotosId(Botaniste botaniste) {
        List<Integer> photos = new ArrayList<>();

        if (botaniste.getPhotos() != null) {
            photos = botaniste.getPhotos().stream()
                    .map(Photo::getId).toList();
        }
        return photos;
    }


    default List<Integer> gardiennageProprietaireIds(Botaniste botaniste) {
        List<Integer> gardesProprioIds = new ArrayList<>();

        if (botaniste.getGardiennagesProprio() != null) {
            gardesProprioIds = botaniste.getGardiennagesProprio().stream()
                    .map(Gardiennage::getId).toList();
        }
        return gardesProprioIds;
    }

    default List<Integer> gardiennageGardienIds(Botaniste botaniste) {
        List<Integer> gardesGardienIds = new ArrayList<>();

        if (botaniste.getGardiennagesGardien() != null) {
            gardesGardienIds = botaniste.getGardiennagesGardien().stream()
                    .map(Gardiennage::getId).toList();
        }
        return gardesGardienIds;
    }

    default List<Photo> getPhotos(BotanisteDto botanisteDto) {
        List<Photo> photos = new ArrayList<>();

        if (botanisteDto.getPhotosIds() != null) {
            photos = botanisteDto.getPhotosIds().stream()
                    .map(Photo::new).toList();
        }
        return photos;
    }

    default List<Gardiennage> getGardiennageProprio(BotanisteDto botanisteDto) {
        List<Gardiennage> gardesProprio = new ArrayList<>();

        if (botanisteDto.getPhotosIds() != null) {
            gardesProprio = botanisteDto.getGardiennageProprietaireIds().stream()
                    .map(Gardiennage::new).toList();
        }
        return gardesProprio;
    }

    default List<Gardiennage> getGardiennageGardien(BotanisteDto botanisteDto) {
        List<Gardiennage> gardiennagesGardien = new ArrayList<>();

        if (botanisteDto.getPhotosIds() != null) {
            gardiennagesGardien = botanisteDto.getGardiennageGardienIds().stream()
                    .map(Gardiennage::new).toList();
        }
        return gardiennagesGardien;
    }


    @Mapping(target = "photos", expression = "java(getPhotos(botanisteDto))")
    @Mapping(target = "gardiennagesProprio", expression = "java(getGardiennageProprio(botanisteDto))")
    @Mapping(target = "gardiennagesGardien", expression = "java(getGardiennageGardien(botanisteDto))")
    Botaniste mapToModel(BotanisteDto botanisteDto);
}
