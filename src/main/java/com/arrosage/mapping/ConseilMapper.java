package com.arrosage.mapping;

import com.arrosage.dto.ConseilDto;
import com.arrosage.model.Conseil;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface ConseilMapper {
    @Mapping(source = "botaniste.id", target = "botanisteId")
    @Mapping(source = "photo.id", target = "photoId")
    ConseilDto mapToDto(Conseil conseil);


    @Mapping(source = "botanisteId", target = "botaniste.id")
    @Mapping(source = "photoId", target = "photo.id")
    Conseil mapToModel(ConseilDto conseilDto);
}
