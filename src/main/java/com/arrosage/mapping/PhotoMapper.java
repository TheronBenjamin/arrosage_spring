package com.arrosage.mapping;

import com.arrosage.dto.PhotoDto;
import com.arrosage.model.Conseil;
import com.arrosage.model.Photo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface PhotoMapper {

    @Mapping(target = "conseilsAvis", expression = "java(getConseilAvis(photo))")
    @Mapping(target = "conseilsIds", expression = "java(getConseilIds(photo))")
    @Mapping(source = "photographe.mail", target = "photographeMail")
    @Mapping(source = "photographe.id", target = "photographeId")
    @Mapping(source = "plante.denomination", target = "planteNom")
    @Mapping(source = "plante.id", target = "planteId")
    PhotoDto mapToDto(Photo photo);

    default List<String> getConseilAvis(Photo photo) {
        List<String> conseils = new ArrayList<>();

        if (photo.getConseils() != null) {
            conseils = photo.getConseils().stream()
                    .map(conseil -> conseil.getAvis()).toList();
        }
        return conseils;
    }

    default List<Integer> getConseilIds(Photo photo) {
        List<Integer> conseils = new ArrayList<>();

        if (photo.getConseils() != null) {
            conseils = photo.getConseils().stream()
                    .map(conseil -> conseil.getId()).toList();
        }
        return conseils;
    }

    default List<Conseil> getConseils(PhotoDto photoDto) {
        List<Conseil> conseils = new ArrayList<>();

        if (photoDto.getConseilsIds() != null) {
            conseils = photoDto.getConseilsIds().stream()
                    .map(id->new Conseil(id)).toList();
        }
        return conseils;
    }


    @Mapping(source = "photographeMail", target = "photographe.mail")
    @Mapping(source = "photographeId", target = "photographe.id")
    @Mapping(source = "planteNom", target = "plante.denomination")
    @Mapping(source = "planteId", target = "plante.id")
    @Mapping(target = "conseils", expression = "java(getConseils(photoDto))")
    Photo mapToModel(PhotoDto photoDto);
}
