package com.arrosage.mapping;

import com.arrosage.dto.UtilisateurDto;
import com.arrosage.model.Gardiennage;
import com.arrosage.model.Photo;
import com.arrosage.model.Utilisateur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
@Mapper(componentModel = "spring")
public interface UtilisateurMapper {

    @Mapping(target = "password", ignore = true)
    @Mapping(target = "gardiennageProprietaireIds", expression = "java(gardiennageProprietaireIds(user))")
    @Mapping(target = "gardiennageGardienIds", expression = "java(gardiennageGardienIds(user))")
    @Mapping(target = "photosIds", expression = "java(getPhotosId(user))")
    UtilisateurDto mapToDto(Utilisateur user);

    default List<Integer> getPhotosId(Utilisateur user) {
        List<Integer> photos = new ArrayList<>();

        if (user.getPhotos() != null) {
            photos = user.getPhotos().stream()
                    .map(Photo::getId).toList();
        }
        return photos;
    }


    default List<Integer> gardiennageProprietaireIds(Utilisateur user) {
        List<Integer> gardesProprioIds = new ArrayList<>();

        if (user.getGardiennagesProprio() != null) {
            gardesProprioIds = user.getGardiennagesProprio().stream()
                    .map(Gardiennage::getId).toList();
        }
        return gardesProprioIds;
    }

    default List<Integer> gardiennageGardienIds(Utilisateur user) {
        List<Integer> gardesGardienIds = new ArrayList<>();

        if (user.getGardiennagesGardien() != null) {
            gardesGardienIds = user.getGardiennagesGardien().stream()
                    .map(Gardiennage::getId).toList();
        }
        return gardesGardienIds;
    }

    default List<Photo> getPhotos(UtilisateurDto userDto) {
        List<Photo> photos = new ArrayList<>();

        if (userDto.getPhotosIds() != null) {
            photos = userDto.getPhotosIds().stream()
                    .map(Photo::new).toList();
        }
        return photos;
    }

    default List<Gardiennage> getGardiennageProprio(UtilisateurDto userDto) {
        List<Gardiennage> gardesProprio = new ArrayList<>();

        if (userDto.getPhotosIds() != null) {
            gardesProprio = userDto.getGardiennageProprietaireIds().stream()
                    .map(Gardiennage::new).toList();
        }
        return gardesProprio;
    }

    default List<Gardiennage> getGardiennageGardien(UtilisateurDto userDto) {
        List<Gardiennage> gardiennagesGardien = new ArrayList<>();

        if (userDto.getPhotosIds() != null) {
            gardiennagesGardien = userDto.getGardiennageGardienIds().stream()
                    .map(Gardiennage::new).toList();
        }
        return gardiennagesGardien;
    }



    @Mapping(target = "photos", expression = "java(getPhotos(userDto))")
    @Mapping(target = "gardiennagesProprio", expression = "java(getGardiennageProprio(userDto))")
    @Mapping(target = "gardiennagesGardien", expression = "java(getGardiennageGardien(userDto))")
    Utilisateur mapToModel(UtilisateurDto userDto);



}
