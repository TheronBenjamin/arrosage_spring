package com.arrosage.mapping;

import com.arrosage.dto.MessageDto;
import com.arrosage.model.Message;
import com.arrosage.model.Utilisateur;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface MessageMapper {

    @Mapping(source = "emeteur.id", target = "emeteur")
    @Mapping(source = "recepteur.id", target = "recepteur")
    MessageDto mapToDto(Message message);

    @Mapping(source = "emeteur", target = "emeteur.id")
    @Mapping(source = "recepteur", target = "recepteur.id")
    Message mapToModel(MessageDto messageDto);

}
