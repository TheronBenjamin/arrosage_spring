package com.arrosage.mapping;

import com.arrosage.dto.GardiennageDto;
import com.arrosage.model.Gardiennage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface GardiennageMapper {
    @Mapping(source = "proprietaire.id", target = "proprietaireId")
    @Mapping(source = "proprietaire.mail", target = "proprietaireMail")
    @Mapping(source = "gardien.id", target = "gardienId")
    @Mapping(source = "gardien.mail", target = "gardienMail")
    GardiennageDto mapToDto(Gardiennage gardiennage);


    @Mapping(source = "proprietaireId", target = "proprietaire.id")
    @Mapping(source = "proprietaireMail", target = "proprietaire.mail")
    @Mapping(source = "gardienId", target = "gardien.id")
    @Mapping(source = "gardienMail", target = "gardien.mail")
    Gardiennage mapToModel(GardiennageDto gardiennageDto);
}
