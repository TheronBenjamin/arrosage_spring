package com.arrosage.mapping;

import com.arrosage.dto.PlanteDto;
import com.arrosage.model.Gardiennage;
import com.arrosage.model.Photo;
import com.arrosage.model.Plante;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface PlanteMapper {

    @Mapping(target = "photosIds", expression = "java(getPhotosId(plante))")
    @Mapping(target = "gardiennageIds", expression = "java(getGardiennagesId(plante))")
    PlanteDto mapToDto(Plante plante);


    default List<Integer> getPhotosId(Plante plante) {
        List<Integer> photos = new ArrayList<>();

        if (plante.getPhotos() != null) {
            photos = plante.getPhotos().stream()
                    .map(Photo::getId).toList();
        }
        return photos;
    }

    default List<Integer> getGardiennagesId(Plante plante) {
        List<Integer> gardiennages = new ArrayList<>();

        if (plante.getGardes() != null) {
            gardiennages = plante.getGardes().stream()
                    .map(Gardiennage::getId).toList();
        }
        return gardiennages;
    }
    default List<Photo> getPhotos(PlanteDto planteDto) {
        List<Photo> photos = new ArrayList<>();

        if (planteDto.getPhotosIds() != null) {
            photos = planteDto.getPhotosIds().stream()
                    .map(Photo::new).toList();
        }
        return photos;
    }

    default List<Gardiennage> getGardiennage(PlanteDto planteDto) {
        List<Gardiennage> gardiennages = new ArrayList<>();

        if (planteDto.getGardiennageIds() != null) {
            gardiennages = planteDto.getGardiennageIds().stream()
                    .map(Gardiennage::new).toList();
        }
        return gardiennages;
    }

    @Mapping(target = "photos", expression = "java(getPhotos(planteDto))")
    @Mapping(target = "gardes", expression = "java(getGardiennage(planteDto))")
    Plante mapToModel(PlanteDto planteDto);
}
