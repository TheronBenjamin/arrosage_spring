package com.arrosage.mapping;

import com.arrosage.dto.AdresseDto;
import com.arrosage.model.Adresse;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface AdresseMapper {
    AdresseDto mapToDto(Adresse adresse);

    Adresse mapToModel(AdresseDto adresseDto);
}
