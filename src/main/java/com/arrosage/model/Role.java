package com.arrosage.model;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_BOTANISTE;
}
