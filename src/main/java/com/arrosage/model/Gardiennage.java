package com.arrosage.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="Gardiennage")
public class Gardiennage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gardiennage_id")
    private Integer id;

    @Column(name = "gardiennage_date_debut")
    private Date dateDebut;

    @Column(name = "gardiennage_date_fin")
    private Date dateFin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gardiennage_proprio")
    private Utilisateur proprietaire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gardiennage_gardien")
    private Utilisateur gardien;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plante_id")
    private Plante plante;

    public Gardiennage(Integer id) {
        this.id=id;
    }
}
