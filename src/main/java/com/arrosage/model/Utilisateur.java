package com.arrosage.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "Utilisateur")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "utilisateur_role")
@DiscriminatorValue("ROLE_USER")
public class Utilisateur {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "utilisateur_mail", unique = true)
    private String mail;

    @Column(name = "utilisateur_login")
    private String login;

    @Column(name = "utilisateur_password")
    private String password;

    @Column(name = "utilisateur_role", length = 50)
    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "proprietaire")
    private List<Gardiennage> gardiennagesProprio;

    @OneToMany(mappedBy = "gardien")
    private List<Gardiennage> gardiennagesGardien;

    @OneToMany(mappedBy = "photographe")
    private List<Photo> photos ;

    @OneToMany
    private List<Message> messages ;

}
