package com.arrosage.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="Conseil")
public class Conseil {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "conseil_id")
    private Integer id;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "botaniste_id")
    private Botaniste botaniste;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "photo_id")
    private Photo photo;


    @Column(name = "conseil_avis")
    private String avis;

    public Conseil(Integer id) {
        this.id = id;
    }
}
