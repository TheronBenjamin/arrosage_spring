package com.arrosage.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="Plante")
public class Plante {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "plante_id")
    private Integer id;

    @Column(name = "plante_nom")
    private String denomination;


    @Column(name = "plante_categorie")
    private String categorie;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "adresse_id", nullable = false)
    private Adresse adresse;


    @OneToMany(mappedBy = "plante")
    private List<Gardiennage> gardes;

    @OneToMany(mappedBy = "plante")
    private List<Photo> photos ;
}
