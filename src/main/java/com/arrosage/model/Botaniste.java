package com.arrosage.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Botaniste")
@DiscriminatorValue("ROLE_BOTANISTE")
public class Botaniste extends Utilisateur {

    @Column(name = "botaniste_specialite")
    private String specialite;

    @OneToMany(mappedBy = "botaniste")
    private List<Conseil> conseils;
}
