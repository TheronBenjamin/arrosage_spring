package com.arrosage.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="Photo")
public class Photo {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private Integer id;


    @Column(name = "photo_descriptif")
    private String descriptif;


    @Column(name = "photo_date")
    private Date datePrisePhoto;


    @Column(name = "photo_image")
    @Lob
    @Type(type="org.hibernate.type.ImageType")
    private byte[] image;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "plante_id")
    private Plante plante;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "utilisateur_id")
    private Utilisateur photographe;

    @OneToMany(mappedBy = "photo")
    private List<Conseil> conseils ;

    public Photo(Integer id) {
        this.id=id;
    }
}
