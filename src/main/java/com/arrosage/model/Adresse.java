package com.arrosage.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="Adresse")
public class Adresse {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "adresse_id")
    private Integer id;


    @Column(name = "adresse_point_x")
    private Double latitude;


    @Column(name = "adresse_point_y")
    private Double longitude;

}
