package com.arrosage.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Table(name="message")
@Entity
@Data
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "date_envoi")
    private Date dateEnvoi = new Date(System.currentTimeMillis());

    private String body;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emeteur")
    private Utilisateur emeteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recepteur")
    private Utilisateur recepteur;

}
