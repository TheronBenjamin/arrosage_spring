Feature: add a plante to gardiennage

  Background: User is Logged In
    Given I navigate to the login page
    When I submit username and password
    Then I should be logged in

  Scenario: A user publish a new gardiennage
    Given A user publish a new gardiennage at "2023-02-23"
    When A user asks for the gardiennage list
    Then he get the list
    And The following gardiennage list should be in the response content
      | gardiennage_date_debut |
      | 2023-02-23             |