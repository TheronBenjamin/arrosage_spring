Feature: Adding a Plante to the catalog

  Background: User is Logged In
    Given I navigate to the login page
    When I submit username and password
    Then I should be logged in

  Scenario: A user publish a new Plante
    Given A user publish a new plante named tulipe with an adress
    When A user asks for the plante list
    Then He should have a success response
    And The following plante list should be in the response content
      | denomination |
      | tulipe       |