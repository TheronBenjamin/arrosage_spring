Feature: Upload an image

  Background: User is Logged In
    Given I navigate to the login page
    When I submit username and password
    Then I should be logged in

  Scenario: Successful image upload
    Given I have an image file "plantes.jfif" in the images folder in ressources directory
    When I create a photo with photoApi at this "2023-02-22"
    Then the response status code should be 201
    And the response body should contain the uploaded image's URL
