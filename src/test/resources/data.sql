


INSERT INTO `utilisateur` (`id`, `utilisateur_mail`, `utilisateur_login`, `utilisateur_password`,
                           `utilisateur_role`)
VALUES (1, 'jeanmichel@wanadoo.fr', 'jeanmich', '$2a$10$ukJ5czLoiZNz34dWlLTb/OvnxEnHfUiZug6/mjN.auC43dHkHxKK6',
        'ROLE_USER');
INSERT INTO `utilisateur` (`id`, `utilisateur_mail`, `utilisateur_login`, `utilisateur_password`, `utilisateur_role`)
VALUES (2, 'pauldubois@fake.fr', 'pauldbs', '$2a$10$ukJ5czLoiZNz34dWlLTb/OvnxEnHfUiZug6/mjN.auC43dHkHxKK6',
        'ROLE_USER');
INSERT INTO `utilisateur` (`id`, `utilisateur_mail`, `utilisateur_login`, `utilisateur_password`, `utilisateur_role`)
VALUES (3, 'jacquesjacques@deepfake.com', 'jackjack', '$2a$10$ukJ5czLoiZNz34dWlLTb/OvnxEnHfUiZug6/mjN.auC43dHkHxKK6',
        'ROLE_BOTANISTE');
INSERT INTO `utilisateur` (`id`, `utilisateur_mail`, `utilisateur_login`, `utilisateur_password`, `utilisateur_role`)
VALUES (4, 'aureliennawac@nimp.fr', 'aurelien1613', '$2a$10$ukJ5czLoiZNz34dWlLTb/OvnxEnHfUiZug6/mjN.auC43dHkHxKK6',
        'ROLE_BOTANISTE');
INSERT INTO `utilisateur` (`id`, `utilisateur_mail`, `utilisateur_login`, `utilisateur_password`, `utilisateur_role`)
VALUES (5, 'henry@wanadoo.fr', 'yrenh', '$2a$10$ukJ5czLoiZNz34dWlLTb/OvnxEnHfUiZug6/mjN.auC43dHkHxKK6', 'ROLE_USER');


INSERT INTO `botaniste` (`id`, `botaniste_specialite`)
VALUES (4, 'Les roses');
INSERT INTO `botaniste` (`id`, `botaniste_specialite`)
VALUES (3, 'Les plantes intérieures');

INSERT INTO adresse (`adresse_point_x`, `adresse_point_y`)
VALUES ('47.800414', '-2.396428');
INSERT INTO adresse (`adresse_point_x`, `adresse_point_y`)
VALUES ('47.813542', '-2.403112');
INSERT INTO adresse (`adresse_point_x`, `adresse_point_y`)
VALUES ('48.145767', '-3.929381');
INSERT INTO adresse (`adresse_point_x`, `adresse_point_y`)
VALUES ('49.024639', '0.767139');

INSERT INTO `plante` (`plante_nom`, `plante_categorie`, `adresse_id`)
VALUES ('roses des bois', 'roses', '1');
INSERT INTO `plante` (`plante_nom`, `plante_categorie`, `adresse_id`)
VALUES ('tulipes', 'plantes', '2');
INSERT INTO `plante` (`plante_nom`, `plante_categorie`, `adresse_id`)
VALUES ('cactus', 'cactus', '3');
INSERT INTO `plante` (`plante_nom`, `plante_categorie`, `adresse_id`)
VALUES ('fougère', 'robustes', '4');

INSERT INTO `photo` (`photo_descriptif`, `photo_date`, `photo_image`, `plante_id`, `utilisateur_id`)
VALUES ('magnifique rosier du balcon', '2022-11-08', NULL, '1', '1');
INSERT INTO `photo` (`photo_descriptif`, `photo_date`, `photo_image`, `plante_id`, `utilisateur_id`)
VALUES ('magnifique rosier du balcon j3', '2022-11-11', NULL, '1', '2');
INSERT INTO `photo` (`photo_descriptif`, `photo_date`, `photo_image`, `plante_id`, `utilisateur_id`)
VALUES ('magnifique rosier du balcon j6', '2022-11-14', NULL, '1', '2');
INSERT INTO `photo` (`photo_descriptif`, `photo_date`, `photo_image`, `plante_id`, `utilisateur_id`)
VALUES ('magnifique rosier du balcon j9', '2022-11-17', NULL, '1', '2');
INSERT INTO `photo` (`photo_descriptif`, `photo_date`, `photo_image`, `plante_id`, `utilisateur_id`)
VALUES ('magnifique rosier du balcon j12', '2022-11-20', NULL, '1', '2');
INSERT INTO `photo` (`photo_descriptif`, `photo_date`, `photo_image`, `plante_id`, `utilisateur_id`)
VALUES ('magnifique rosier du balcon j15', '2022-11-23', NULL, '1', '2');
INSERT INTO `photo` (`photo_descriptif`, `photo_date`, `photo_image`, `plante_id`, `utilisateur_id`)
VALUES ('magnifique fougère', '2022-11-08', NULL, '4', '5');

INSERT INTO `conseil` (`botaniste_id`, `photo_id`, `conseil_avis`)
VALUES ('4', '2', 'bien arrosé mais pas trop non plus!');
INSERT INTO `conseil` (`botaniste_id`, `photo_id`, `conseil_avis`)
VALUES ('3', '6', 'Ne rien faire cette plante est très bien toute seule');

INSERT INTO `gardiennage` (`plante_id`, `gardiennage_date_debut`, `gardiennage_date_fin`, `gardiennage_proprio`,
                           `gardiennage_gardien`)
VALUES ('1', '2022-11-08', '2022-11-23', '1', '5');
