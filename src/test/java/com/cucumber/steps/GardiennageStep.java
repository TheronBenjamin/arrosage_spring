package com.cucumber.steps;

import com.arrosage.dto.AdresseDto;
import com.arrosage.dto.GardiennageDto;
import com.arrosage.dto.PlanteDto;
import com.arrosage.model.Gardiennage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.cucumber.steps.AuthenticateStep.token;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class GardiennageStep {


    private static Response response;

    @LocalServerPort
    private int port;

    private final String directory = "images/";

    private byte[] filesImages;

    @Before
    public void setUp() {
        RestAssured.baseURI = "http://localhost";

        RestAssured.port = port;
    }


    @Given("A user publish a new gardiennage at {string}")
    public void aUserPublishNewGardiennage(String date) throws ParseException, JsonProcessingException {
        RequestSpecification request = RestAssured.given();
        AdresseDto adresseDto = new AdresseDto(null, 156163.210, 161565.2);
        List<Integer> photosId = new ArrayList<>();
        photosId.add(1);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
        Date dateDto = format.parse(date);
        PlanteDto planteDto = new PlanteDto(1, "rose", null, adresseDto, null, photosId);
        GardiennageDto gardiennageDto = new GardiennageDto(null, dateDto, null, 1, "jeanmichel@wanadoo.fr", null, null, planteDto);


        ObjectMapper mapper = new ObjectMapper();

        String jsonString = mapper.writeValueAsString(gardiennageDto);

        System.out.println("------------------------------------");
        System.out.println(jsonString);

        response = request.headers(
                        "Authorization",
                        "Bearer " + token,
                        "Content-Type",
                        ContentType.JSON,
                        "Accept",
                        ContentType.JSON)
                .body(jsonString)
                .post("/api/gardiennages");

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.SC_CREATED);
    }


    @When("A user asks for the gardiennage list")
    public void aUserAsksForTheGardiennageList() {
        RequestSpecification request = RestAssured.given();
        request.headers(
                "Authorization",
                "Bearer " + token,
                "Content-Type",
                ContentType.JSON,
                "Accept",
                ContentType.JSON);

        response = request.get("/api/gardiennages");
    }

    @Then("he get the list")
    public void heShouldHaveASuccessResponse() {
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.SC_OK);
    }

    @And("The following gardiennage list should be in the response content")
    public void contentResponseWithGardiennage(DataTable table) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        String serializedGardiennage = response.getBody().asString();
        System.out.println("____________________________________");
        System.out.println(serializedGardiennage);
        List<Gardiennage> actualGardiennages = mapper.readValue(serializedGardiennage, new TypeReference<>() {
        });

        List<String> expectedGardiennageDates = table.asList().subList(2, table.height());


        for (String expectedGardiennageDate : expectedGardiennageDates) {
            boolean dateFound = actualGardiennages.stream()
                    .anyMatch(gardiennage -> gardiennage.getDateDebut().toString().equals(expectedGardiennageDate));
            assertThat(dateFound).isTrue();
        }


    }

}
