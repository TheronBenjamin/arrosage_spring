package com.cucumber.steps;


import com.arrosage.dto.AdresseDto;
import com.arrosage.dto.PlanteDto;
import com.arrosage.model.Plante;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.List;

import static com.cucumber.steps.AuthenticateStep.token;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PlanteApiStepDef {


    private static Response response;

    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        RestAssured.baseURI = "http://localhost";

        RestAssured.port = port;
    }

    @When("A user asks for the plante list")
    public void aUserAsksForThePlanteList() {
        RequestSpecification request = RestAssured.given();
        request.headers(
                "Authorization",
                "Bearer " + token,
                "Content-Type",
                ContentType.JSON,
                "Accept",
                ContentType.JSON);

        response = request.get("/api/plantes");
    }

    @Then("He should have a success response")
    public void heShouldHaveASuccessResponse() {
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.SC_OK);
    }

    @Given("A user publish a new plante named {word} with an adress")
    public void aUserPublishANewPlante(String name) throws JsonProcessingException {
        RequestSpecification request = RestAssured.given();
        AdresseDto adresseDto = new AdresseDto(null, 156163.210, 161565.2);
        PlanteDto planteDto = new PlanteDto(null, name, null, adresseDto, null, null);


        ObjectMapper mapper = new ObjectMapper();

        String jsonString = mapper.writeValueAsString(planteDto);

        response = request.headers(
                        "Authorization",
                        "Bearer " + token,
                        "Content-Type",
                        ContentType.JSON,
                        "Accept",
                        ContentType.JSON)
                .body(jsonString)
                .post("/api/plantes");

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.SC_CREATED);
    }

    @And("The following plante list should be in the response content")
    public void theFollowingPlanteShouldBeInTheResponseContent(DataTable table) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        String serializedPlante = response.getBody().asString();
        System.out.println("____________________________________");
        System.out.println(serializedPlante);
        List<Plante> actualPlantes = mapper.readValue(serializedPlante, new TypeReference<>() {
        });

        List<String> expectedPlanteDenominations = table.asList().subList(1, table.height());
        System.out.println(expectedPlanteDenominations.get(0));
        for (String expectedPlanteDenomination : expectedPlanteDenominations) {
            boolean nameFound = actualPlantes.stream()
                    .anyMatch(plante -> plante.getDenomination().equals(expectedPlanteDenomination));
            assertThat(nameFound).isTrue();
        }


    }


}
