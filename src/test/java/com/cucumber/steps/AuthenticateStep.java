package com.cucumber.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AuthenticateStep {

    private static final String USER_ID = "1";
    private static final String MAIL = "jeanmichel@wanadoo.fr";
    private static final String PASSWORD = "1234";
    private static final String BASE_URL = "http://localhost";

    public static String token;
    private static Response response;
    private static String jsonString;
    private static String bookId;


    @Given("I navigate to the login page")
    public void iAmAnAuthorizedUser() {

        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        response = request.get("/");
    }

    @When("I submit username and password")
    public void submitAuthenticate() {
        RequestSpecification request = RestAssured.given();

        request.header("Content-Type", "application/json");
        //response = request.body("{\"mail\":\"jeanmichel@wanadoo.fr\",\"password\":\"1234\"}")
        //      .post("/api/users/authenticate");

        response = request.body("{\"mail\":\"aureliennawac@nimp.fr\",\"password\":\"1234\"}")
                .post("/api/users/authenticate");


        String jsonString = response.asString();


        token = JsonPath.from(jsonString).get("token");

    }

    @Then("I should be logged in")
    public void heShouldHaveASuccessResponse() {
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.SC_OK);
    }
}
