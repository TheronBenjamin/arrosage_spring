package com.cucumber.steps;

import com.arrosage.dto.PhotoDto;
import com.arrosage.model.Photo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static com.cucumber.steps.AuthenticateStep.token;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ImageStep {


    private static Response response;

    @LocalServerPort
    private int port;

    private final String directory = "images/";

    private byte[] filesImages;

    @Before
    public void setUp() {
        RestAssured.baseURI = "http://localhost";

        RestAssured.port = port;
    }


    @Given("I have an image file {string} in the images folder in ressources directory")
    public void iHaveAnImageFileInTheResourcesFolder(String filename) {

        System.out.println(getClass().getResource("/" + directory + filename));
        // Read the file from the resources folder
        File file = new File(Objects.requireNonNull(getClass().getResource("/" + directory + filename)).getFile());

        // Convert the file to a byte array
        byte[] fileBytes = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            fileBytes = new byte[(int) file.length()];
            fis.read(fileBytes);
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save the file bytes to a member variable
        this.filesImages = fileBytes;
    }


    @When("I create a photo with photoApi at this {string}")
    public void aUserPublishANewPhoto(String date) throws JsonProcessingException, ParseException {
        RequestSpecification request = RestAssured.given();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
        Date dateDto = format.parse(date);
        System.out.println("----------------------------------------------------------");
        System.out.println(dateDto);
        System.out.println("----------------------------------------------------------");
        File file = new File(Objects.requireNonNull(getClass().getResource("/" + directory + "plantes.jfif")).getFile());
        PhotoDto photoDto = new PhotoDto(null, "une belle photo", dateDto, filesImages, 1, "fougères", "jeanmichel@wanadoo.fr", 1, null, null);


        ObjectMapper mapper = new ObjectMapper();

        String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(date);

        System.out.println("----------------------------------------------------------");
        System.out.println(jsonString);
        System.out.println("----------------------------------------------------------");

        response = request.given()
                .header("Authorization", "Bearer " + token)
                .header("Accept", ContentType.JSON)
                .multiPart("file", file)
                .formParam("descriptif", photoDto.getDescriptif())
                .formParam("planteId", photoDto.getPlanteId())
                .formParam("date", date)
                .formParam("photographeId", photoDto.getPhotographeId())
                .post("/api/photos");
    }

    @Then("the response status code should be 201")
    public void heShouldHaveASuccessResponse() {
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.SC_CREATED);
    }

    @And("the response body should contain the uploaded image's URL")
    public void contentResponseWithImage() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        String serializedPhoto = response.getBody().asString();
        System.out.println("____________________________________");
        System.out.println(serializedPhoto);
        Photo actualPhoto = mapper.readValue(serializedPhoto, new TypeReference<>() {
        });


        Assertions.assertNotNull(actualPhoto.getImage());
    }


}
