package com.arrosage;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
class ArrosageApplicationTests {

	@Test
	void contextLoads() {
	}

}
