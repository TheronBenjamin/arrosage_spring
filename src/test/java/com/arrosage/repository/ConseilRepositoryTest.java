package com.arrosage.repository;

import com.arrosage.model.Conseil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class ConseilRepositoryTest {

    @Autowired
    ConseilRepository conseilRepository;

    @Test
    void findConseilsByBotanisteIdTest() {
        List<Conseil> conseils = this.conseilRepository.findConseilsByBotanisteId(3);
        Assertions.assertEquals(1, conseils.size());
    }

    @Test
    void findConseilsByBotanisteIdUnknwonTest() {
        List<Conseil> conseils = this.conseilRepository.findConseilsByBotanisteId(2546);
        Assertions.assertEquals(0, conseils.size());
    }

    @Test
    void findConseilsByPhotoIdTest() {
        List<Conseil> conseils = this.conseilRepository.findConseilsByPhotoId(6);
        Assertions.assertEquals(1, conseils.size());
    }

    @Test
    void findConseilsByPhotoIdUnknwonTest() {
        List<Conseil> conseils = this.conseilRepository.findConseilsByPhotoId(4984);
        Assertions.assertEquals(0, conseils.size());
    }
}
