package com.arrosage.repository;

import com.arrosage.model.Gardiennage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class GardiennageRepositoryTest {

    @Autowired
    GardiennageRepository gardiennageRepository;

    @Test
    void findGardiennagesByPlanteIdTest() {
        List<Gardiennage> gardiennages = gardiennageRepository.findGardiennagesByPlanteId(1);
        Assertions.assertEquals(1, gardiennages.size());
    }


    @Test
    void findGardiennagesByPlanteIdUnknownTest() {
        List<Gardiennage> gardiennages = gardiennageRepository.findGardiennagesByPlanteId(11516);
        Assertions.assertEquals(0, gardiennages.size());
    }

    @Test
    void findGardiennagesByProprietaireIdTest() {
        List<Gardiennage> gardiennages = gardiennageRepository.findGardiennagesByProprietaireId(1);
        Assertions.assertEquals(1, gardiennages.size());
    }

    @Test
    void findGardiennagesByProprietaireIdUnknownTest() {
        List<Gardiennage> gardiennages = gardiennageRepository.findGardiennagesByProprietaireId(1156);
        Assertions.assertEquals(0, gardiennages.size());
    }

    @Test
    void findGardiennagesByGardienIdTest() {
        List<Gardiennage> gardiennages = gardiennageRepository.findGardiennagesByGardienId(5);
        Assertions.assertEquals(1, gardiennages.size());
    }

    @Test
    void findGardiennagesByGardienIdUnknownTest() {
        List<Gardiennage> gardiennages = gardiennageRepository.findGardiennagesByGardienId(4165);
        Assertions.assertEquals(0, gardiennages.size());
    }

}
