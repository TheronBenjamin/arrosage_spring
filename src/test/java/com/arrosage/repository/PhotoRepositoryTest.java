package com.arrosage.repository;

import com.arrosage.model.Photo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class PhotoRepositoryTest {

    @Autowired
    PhotoRepository photoRepository;

    @Test
    void findPhotosByPhotographeIdTest() {
        List<Photo> photos = photoRepository.findPhotosByPhotographeId(1);
        Assertions.assertEquals(1, photos.size());
    }

    @Test
    void findPhotosByPlanteIdTest() {
        List<Photo> photos = photoRepository.findPhotosByPlanteId(1);
        Assertions.assertEquals(6, photos.size());

    }
}
