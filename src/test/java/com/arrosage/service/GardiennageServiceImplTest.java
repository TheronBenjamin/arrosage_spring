package com.arrosage.service;

import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageDateException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageProprioException;
import com.arrosage.exceptions.gardiennage.exception.GardiennageUnknownException;
import com.arrosage.model.Gardiennage;
import com.arrosage.model.Plante;
import com.arrosage.model.Role;
import com.arrosage.model.Utilisateur;
import com.arrosage.repository.GardiennageRepository;
import com.arrosage.repository.PlanteRepository;
import com.arrosage.repository.UtilisateurRepository;
import com.arrosage.service.impl.GardiennageServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@ExtendWith(MockitoExtension.class)
class GardiennageServiceImplTest {

    @Mock
    GardiennageRepository gardiennageRepository;

    @Mock
    UtilisateurRepository utilisateurRepository;

    @Mock
    PlanteRepository planteRepository;

    @InjectMocks
    GardiennageServiceImpl gardiennageService;

    @Test
    void getByIdTest() throws GardiennageUnknownException {
        Gardiennage gardiennage = new Gardiennage(1);
        Mockito.when(gardiennageRepository.getReferenceById(1)).thenReturn(gardiennage);

        Gardiennage g2 = gardiennageService.getById(1);
        Assertions.assertEquals(g2.getId(), gardiennage.getId());
    }

    @Test
    void getByIdTestKo() {

        Mockito.when(gardiennageRepository.getReferenceById(1)).thenThrow(new EntityNotFoundException());

        Assertions.assertThrows(GardiennageUnknownException.class, () -> gardiennageService.getById(1));
    }


    @Test
    void createGardiennageTest() {
        Gardiennage gardiennage = this.initGardiennage();
        Mockito.when(utilisateurRepository.getReferenceById(gardiennage.getProprietaire().getId())).thenReturn(gardiennage.getProprietaire());
        Mockito.when(planteRepository.getReferenceById(gardiennage.getPlante().getId())).thenReturn(gardiennage.getPlante());

        try {
            gardiennageService.createGardiennage(gardiennage);
            ArgumentCaptor<Gardiennage> captor = ArgumentCaptor.forClass(Gardiennage.class);
            Mockito.verify(gardiennageRepository).save(captor.capture());
            Gardiennage savedGardiennage = captor.getValue();
            Assertions.assertEquals(savedGardiennage.getProprietaire().getLogin(), gardiennage.getProprietaire().getLogin());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void createGardiennageTestPlanteKo() {
        Gardiennage gardiennage = this.initGardiennage();
        gardiennage.setPlante(null);
        Mockito.when(utilisateurRepository.getReferenceById(gardiennage.getProprietaire().getId())).thenReturn(gardiennage.getProprietaire());
        Assertions.assertThrows(PlanteUnknownException.class, () -> gardiennageService.createGardiennage(gardiennage));
    }

    @Test
    void createGardiennageTestDateKo() {
        Gardiennage gardiennage = this.initGardiennage();
        gardiennage.setDateDebut(null);
        Assertions.assertThrows(GardiennageDateException.class, () -> gardiennageService.createGardiennage(gardiennage));
    }

    @Test
    void createGardiennageTestProprioKo() {
        Gardiennage gardiennage = this.initGardiennage();
        gardiennage.setProprietaire(null);
        Assertions.assertThrows(GardiennageProprioException.class, () -> gardiennageService.createGardiennage(gardiennage));
    }

    @Test
    void updateGardiennageTest() {
        Gardiennage gardiennage = this.initGardiennage();
        Gardiennage gardiennageUpdate = this.initGardiennage();
        gardiennageUpdate.setDateFin(Date.from(Instant.now().plus(12, ChronoUnit.DAYS)));
        Mockito.when(gardiennageRepository.getReferenceById(gardiennage.getId())).thenReturn(gardiennageUpdate);
        try {
            gardiennage.setDateFin(Date.from(Instant.now().plus(12, ChronoUnit.DAYS)));
            gardiennageService.updateGardiennage(gardiennage);
            Assertions.assertEquals(gardiennageUpdate.getDateFin(), gardiennage.getDateFin());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void updateGardiennageTestDateKo() {
        Gardiennage gardiennage = this.initGardiennage();
        Gardiennage gardiennageUpdate = this.initGardiennage();
        gardiennageUpdate.setDateFin(Date.from(Instant.now().plus(12, ChronoUnit.DAYS)));
        Mockito.when(gardiennageRepository.getReferenceById(gardiennage.getId())).thenReturn(gardiennageUpdate);
        
        gardiennage.setDateFin(Date.from(Instant.now().minus(12, ChronoUnit.DAYS)));

        Assertions.assertThrows(GardiennageDateException.class, () -> gardiennageService.updateGardiennage(gardiennage));


    }

    private Gardiennage initGardiennage() {
        Gardiennage gardiennage = new Gardiennage();
        gardiennage.setId(1);
        Utilisateur proprio = new Utilisateur(1, "test", "test", "test", Role.ROLE_USER, null, null, null,null);
        gardiennage.setProprietaire(proprio);
        gardiennage.setDateDebut(Date.from(Instant.now().minus(10, ChronoUnit.DAYS)));
        Plante plante = new Plante();
        gardiennage.setPlante(plante);
        return gardiennage;
    }
}
