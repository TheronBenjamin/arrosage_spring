package com.arrosage.service;

import com.arrosage.exceptions.AdresseException;
import com.arrosage.model.Adresse;
import com.arrosage.model.Plante;
import com.arrosage.repository.AdresseRepository;
import com.arrosage.service.impl.AdresseServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;

@ExtendWith(MockitoExtension.class)
class AdresseServiceImplTest {

    @Mock
    AdresseRepository adresseRepository;

    @InjectMocks
    AdresseServiceImpl adresseService;

    @Test
    void getByIdTest() throws AdresseException {
        Adresse adresse = new Adresse();
        adresse.setId(1);
        Mockito.when(adresseRepository.getReferenceById(1)).thenReturn(adresse);

        Adresse a2 = adresseService.getById(1);
        Assertions.assertEquals(a2.getId(), adresse.getId());
    }

    @Test
    void getByIdTestKo() {
        Mockito.when(adresseRepository.getReferenceById(1)).thenThrow(new EntityNotFoundException());
        Assertions.assertThrows(AdresseException.class, () -> adresseService.getById(1));
    }

    @Test
    void createAdresseTest() throws AdresseException {
        Plante plante = new Plante();
        plante.setId(1);
        Adresse adresse = new Adresse(1, +1568.1616, -1.1616);

        Mockito.when(adresseRepository.save(adresse)).thenReturn(adresse);
        Adresse adresse1 = this.adresseService.createAdresse(adresse);
        Assertions.assertEquals(adresse.getLatitude(), adresse1.getLatitude());

    }

    @Test
    void createAdresseTestLattitudeKo() {
        Adresse adresse = new Adresse();
        adresse.setLatitude(null);

        Assertions.assertThrows(AdresseException.class, () -> this.adresseService.createAdresse(adresse));

    }
}
