package com.arrosage.service;

import com.arrosage.exceptions.photo.exception.PhotoEmptyException;
import com.arrosage.model.Adresse;
import com.arrosage.model.Gardiennage;
import com.arrosage.model.Photo;
import com.arrosage.model.Plante;
import com.arrosage.repository.PlanteRepository;
import com.arrosage.service.impl.AdresseServiceImpl;
import com.arrosage.service.impl.GardiennageServiceImpl;
import com.arrosage.service.impl.PhotoServiceImpl;
import com.arrosage.service.impl.PlanteServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class PlanteServiceImplTest {

    private final String adressException = "l'adresse n'est pas renseignée";

    @Mock
    PlanteRepository planteRepository;

    @Mock
    AdresseServiceImpl adresseService;

    @Mock
    PhotoServiceImpl photoService;

    @Mock
    GardiennageServiceImpl gardiennageService;

    @InjectMocks
    PlanteServiceImpl planteService;


    @Test
    void getByIdTest() {
        Plante plante = this.initPlanteOk();
        Mockito.when(planteRepository.getReferenceById(1)).thenReturn(plante);

        Plante p2 = planteService.getById(1);
        Assertions.assertEquals(p2.getDenomination(), plante.getDenomination());
    }

    @Test
    void getByIdTestKo() {
        Mockito.when(planteRepository.getReferenceById(15)).thenThrow(new EntityNotFoundException());
        Assertions.assertThrows(EntityNotFoundException.class, () -> planteService.getById(15));
    }


    @Test
    void testCreatePlante() throws Exception {
        Plante plante = this.initPlanteOk();
        Mockito.when(adresseService.createAdresse(plante.getAdresse())).thenReturn(plante.getAdresse());

        try {
            planteService.createPlante(plante);
            ArgumentCaptor<Plante> captor = ArgumentCaptor.forClass(Plante.class);
            Mockito.verify(planteRepository).save(captor.capture());
            Plante savedPlante = captor.getValue();
            Assertions.assertEquals(savedPlante.getDenomination(), plante.getDenomination());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testCreatePlanteWithPhoto() throws Exception {
        Plante plante = this.initCompletePlanteOk();
        Mockito.when(adresseService.createAdresse(plante.getAdresse())).thenReturn(plante.getAdresse());

        try {
            planteService.createPlante(plante);
            ArgumentCaptor<Plante> captor = ArgumentCaptor.forClass(Plante.class);
            Mockito.verify(planteRepository).save(captor.capture());
            Plante savedPlante = captor.getValue();
            Assertions.assertEquals(savedPlante.getDenomination(), plante.getDenomination());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testCreatePlanteAdresseKO() {
        Plante plante = new Plante();
        try {
            planteService.createPlante(plante);
        } catch (Exception e) {
            Assertions.assertEquals(e.getMessage(), adressException);
        }

    }

    @Test
    void testUpdatePlanteBeforeGardes() {
        Plante plante = this.initPlanteOk();
        Adresse adresse = this.initAdresseOk();
        Plante planteUpdate = new Plante(1, "rose", "Fleurs", adresse, null, null);
        Mockito.when(planteRepository.save(plante)).thenReturn(planteUpdate);

        try {
            plante.setDenomination("rose");
            planteService.updatePlante(plante);
            Assertions.assertEquals(plante.getDenomination(), planteUpdate.getDenomination());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    void testUpdatePlanteAfterGardes() {
        Plante plante = this.initCompletePlanteOk();
        Plante planteUpdate = this.initCompletePlanteOk();
        planteUpdate.setCategorie("Rose");
        Mockito.when(planteRepository.save(plante)).thenReturn(planteUpdate);
        Mockito.when(gardiennageService.getGardiennagesByPlante(plante)).thenReturn(plante.getGardes());

        try {
            plante.setCategorie("Rose");
            planteService.updatePlante(plante);
            Assertions.assertEquals(plante.getCategorie(), planteUpdate.getCategorie());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    void testUpdatePlanteAfterGardesWhitoutPhoto() {
        Adresse adresse = this.initAdresseOk();
        List<Gardiennage> gardes = this.initGardesOk();
        Plante planteUpdate = new Plante(1, "rose", "Fleurs", adresse, gardes, null);
        Mockito.when(gardiennageService.getGardiennagesByPlante(planteUpdate)).thenReturn(planteUpdate.getGardes());

        Assertions.assertThrows(PhotoEmptyException.class, () -> planteService.updatePlante(planteUpdate));

    }

    private Plante initPlanteOk() {
        Plante plante = new Plante();
        Adresse adresse = this.initAdresseOk();
        plante.setAdresse(adresse);
        plante.setDenomination("tulipe");
        plante.setCategorie("Fleurs");
        return plante;
    }

    private Plante initCompletePlanteOk() {
        Plante plante = new Plante();
        Adresse adresse = this.initAdresseOk();
        plante.setAdresse(adresse);
        plante.setDenomination("tulipe");
        plante.setCategorie("Fleurs");
        List<Gardiennage> gardes = this.initGardesOk();
        List<Photo> photos = this.initPhotosOk();
        plante.setGardes(gardes);
        plante.setPhotos(photos);
        return plante;
    }

    private Adresse initAdresseOk() {
        Adresse adresse = new Adresse();
        adresse.setId(1);
        adresse.setLatitude(1.0);
        adresse.setLongitude(1.0);
        return adresse;
    }

    private List<Gardiennage> initGardesOk() {
        List<Gardiennage> gardes = new ArrayList<>();
        Gardiennage garde = new Gardiennage(1);
        gardes.add(garde);
        return gardes;
    }

    private List<Photo> initPhotosOk() {
        List<Photo> photos = new ArrayList<>();
        Photo photo = new Photo(1);
        photos.add(photo);
        return photos;
    }

}
