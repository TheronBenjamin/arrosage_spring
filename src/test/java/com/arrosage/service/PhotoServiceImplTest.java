package com.arrosage.service;

import com.arrosage.exceptions.PlanteUnknownException;
import com.arrosage.exceptions.UtilisateurException;
import com.arrosage.exceptions.photo.exception.PhotoDateException;
import com.arrosage.model.Photo;
import com.arrosage.model.Plante;
import com.arrosage.model.Utilisateur;
import com.arrosage.repository.PhotoRepository;
import com.arrosage.repository.PlanteRepository;
import com.arrosage.repository.UtilisateurRepository;
import com.arrosage.service.impl.PhotoServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceImplTest {
    @Mock
    PhotoRepository photoRepository;

    @Mock
    PlanteRepository planteRepository;

    @Mock
    UtilisateurRepository utilisateurRepository;

    @InjectMocks
    PhotoServiceImpl photoService;

    @Test
    void getByIdTest() {
        Photo photo = this.initPhotoOk();
        Mockito.when(photoRepository.getReferenceById(1)).thenReturn(photo);

        Photo p2 = photoService.getById(1);
        Assertions.assertEquals(p2.getDescriptif(), photo.getDescriptif());
    }

    @Test
    void getByIdTestKo() {
        Mockito.when(photoRepository.getReferenceById(15)).thenThrow(new EntityNotFoundException());
        Assertions.assertThrows(EntityNotFoundException.class, () -> photoService.getById(15));
    }

    @Test
    void testCreatePhoto() {
        Photo photo = this.initPhotoOk();
        Mockito.when(planteRepository.getReferenceById(photo.getPlante().getId())).thenReturn(photo.getPlante());
        Mockito.when(utilisateurRepository.getReferenceById(photo.getPhotographe().getId())).thenReturn(photo.getPhotographe());
        try {
            photoService.createPhoto(photo);
            ArgumentCaptor<Photo> captor = ArgumentCaptor.forClass(Photo.class);
            Mockito.verify(photoRepository).save(captor.capture());
            Photo savedPhoto = captor.getValue();
            Assertions.assertEquals(savedPhoto.getDescriptif(), photo.getDescriptif());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testCreatePhotoDateKo() {
        Photo photo = this.initPhotoOk();
        //Date actuelle + 5 min
        photo.setDatePrisePhoto(Date.from(Instant.now().plus(5, ChronoUnit.MINUTES)));
        Assertions.assertThrows(PhotoDateException.class, () -> photoService.createPhoto(photo));
    }

    @Test
    void testCreatePhotoPlanteKo() {
        Photo photo = this.initPhotoOk();
        photo.setPlante(null);
        Assertions.assertThrows(PlanteUnknownException.class, () -> photoService.createPhoto(photo));
    }

    @Test
    void testCreatePhotoUtilisateurKo() {
        Photo photo = this.initPhotoOk();
        photo.setPhotographe(null);
        Assertions.assertThrows(UtilisateurException.class, () -> photoService.createPhoto(photo));
    }

    @Test
    void testupdatePhoto() {
        Photo photo = this.initPhotoOk();

        try {
            photo.setDescriptif("test");
            photoService.updatePhoto(photo);
            ArgumentCaptor<Photo> captor = ArgumentCaptor.forClass(Photo.class);
            Mockito.verify(photoRepository).save(captor.capture());
            Photo savedPhoto = captor.getValue();
            Assertions.assertEquals(savedPhoto.getDescriptif(), photo.getDescriptif());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Photo initPhotoOk() {
        Photo photo = new Photo();
        photo.setId(1);
        photo.setDescriptif("photo d'une rose");
        //Date actuelle - 30 millisecondes
        photo.setDatePrisePhoto(Date.from(Instant.now().minus(30, ChronoUnit.MILLIS)));

        //TODO:Création d'un blob pour simuler l'image de la photo?
        byte[] blob = new byte[100];
        photo.setImage(blob);

        Utilisateur utilisateur = new Utilisateur();
        Plante plante = new Plante();

        photo.setPhotographe(utilisateur);
        photo.setPlante(plante);
        return photo;
    }
}
