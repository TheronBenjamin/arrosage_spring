package com.arrosage.service;

import com.arrosage.exceptions.conseil.exception.ConseilAvisException;
import com.arrosage.exceptions.conseil.exception.ConseilUnknownException;
import com.arrosage.model.Botaniste;
import com.arrosage.model.Conseil;
import com.arrosage.model.Photo;
import com.arrosage.repository.ConseilRepository;
import com.arrosage.service.impl.ConseilServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;

@ExtendWith(MockitoExtension.class)
class ConseilServiceImplTest {

    @Mock
    ConseilRepository conseilRepository;

    @InjectMocks
    ConseilServiceImpl conseilService;

    @Test
    void getByIdTest() throws ConseilUnknownException {
        Conseil conseil = new Conseil();
        conseil.setId(1);
        Mockito.when(conseilRepository.getReferenceById(1)).thenReturn(conseil);

        Conseil c2 = conseilService.getById(1);
        Assertions.assertEquals(c2.getId(), conseil.getId());
    }

    @Test
    void getByIdTestKo() {

        Mockito.when(conseilRepository.getReferenceById(1)).thenThrow(new EntityNotFoundException());

        Assertions.assertThrows(ConseilUnknownException.class, () -> conseilService.getById(1));
    }

    @Test
    void createConseilTest() {
        Conseil conseil = this.initConseil();
        Mockito.when(conseilRepository.save(conseil)).thenReturn(conseil);
        try {
            Conseil conseilAdd = conseilService.createConseil(conseil);
            Assertions.assertEquals(conseilAdd.getAvis(), conseil.getAvis());
        } catch (ConseilAvisException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Test
    void createConseilTestPhotoKo() {
        Conseil conseil = this.initConseil();
        conseil.setPhoto(null);
        Assertions.assertThrows(ConseilAvisException.class, () -> conseilService.createConseil(conseil));

    }

    @Test
    void updateConseilTest() {
        Conseil conseil = this.initConseil();

        Conseil conseilUpdate = this.initConseil();
        conseilUpdate.setAvis("plante ok");
        Mockito.when(conseilRepository.getReferenceById(conseil.getId())).thenReturn(conseil);
        Mockito.when(conseilRepository.save(conseil)).thenReturn(conseilUpdate);


        try {
            conseil.setAvis("plante ok");
            conseilService.updateConseil(conseil);
            Assertions.assertEquals(conseil.getAvis(), conseilUpdate.getAvis());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    void updateConseilTestAvisKo() {
        Conseil conseil = this.initConseil();

        Conseil conseilUpdate = this.initConseil();
        conseilUpdate.setAvis("plante ok");
        Mockito.when(conseilRepository.getReferenceById(conseil.getId())).thenReturn(conseil);

        conseil.setAvis(null);
        Assertions.assertThrows(ConseilAvisException.class, () -> conseilService.updateConseil(conseil));


    }

    private Conseil initConseil() {
        Conseil conseil = new Conseil();
        conseil.setId(1);
        conseil.setAvis("plante en mauvais état");
        Botaniste botaniste = new Botaniste();
        Photo photo = new Photo();
        conseil.setBotaniste(botaniste);
        conseil.setPhoto(photo);
        return conseil;
    }

}
